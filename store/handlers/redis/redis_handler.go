package redis

import (
	"context"
	"fmt"
	"strconv"
	"sync"
	"time"

	"github.com/garyburd/redigo/redis"
	"github.com/golang/protobuf/proto"
	logger "github.com/ipfs/go-log"
	uuid "github.com/satori/go.uuid"
	configurator "gitlab.com/employeedb/configurator/pb"
	"gitlab.com/employeedb/store/item"
)

var log = logger.Logger("store/redis")

type redisHandler struct {
	conf *configurator.DBConfig
	pool *redis.Pool
}

func NewRedisStore(conf *configurator.DBConfig) (*redisHandler, error) {
	store := new(redisHandler)
	store.conf = conf
	store.pool = &redis.Pool{
		MaxIdle:     3,
		IdleTimeout: 240 * time.Second,

		Dial: func() (redis.Conn, error) {
			c, err := redis.Dial("tcp", store.conf.Hostname+":"+strconv.Itoa(int(store.conf.Port)))
			if err != nil {
				return nil, err
			}
			return c, err
		},

		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			_, err := c.Do("PING")
			return err
		},
	}
	return store, nil
}

func (r *redisHandler) storeKey(i item.Item) string {
	if len(r.conf.DbName) > 0 {
		return r.conf.DbName + "_" + i.GetNamespace() + "_" + i.GetId()
	}
	return i.GetNamespace() + "_" + i.GetId()
}

func (r *redisHandler) Create(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	if v, ok := i.(item.IdSetter); ok {
		v.SetId(uuid.NewV4().String())
	}

	if v, ok := i.(item.TimeTracker); ok {
		v.SetCreated(time.Now().Unix())
		v.SetUpdated(time.Now().Unix())
		_, err := _conn.Do("ZADD", "created_"+i.GetNamespace(),
			v.GetCreated(), r.storeKey(i))
		if err != nil {
			return fmt.Errorf("error creating created time index %s: %v",
				r.storeKey(i), err)
		}
		_, err = _conn.Do("ZADD", "updated_"+i.GetNamespace(),
			v.GetUpdated(), r.storeKey(i))
		if err != nil {
			return fmt.Errorf("error creating updated time index %s: %v",
				r.storeKey(i), err)
		}
		log.Info("Added created and updated index")
	}

	if msg, ok := i.(proto.Message); ok {
		val, err := proto.Marshal(msg)
		if err != nil {
			return fmt.Errorf("error marshalling key %s: %v", r.storeKey(i), err)
		}

		_, err = _conn.Do("SET", r.storeKey(i), val)
		if err != nil {
			v := string(val)
			if len(v) > 15 {
				v = v[0:12] + "..."
			}
			return fmt.Errorf("error setting key %s to %s: %v", r.storeKey(i), v, err)
		}
		return nil
	}

	return fmt.Errorf("unsupported object type: %v", i)
}

func (r *redisHandler) Update(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	if v, ok := i.(item.TimeTracker); ok {
		v.SetUpdated(time.Now().Unix())
		_, err := _conn.Do("ZREM", "updated_"+i.GetNamespace(), r.storeKey(i))
		if err != nil {
			return fmt.Errorf("error removing updated time index %s: %v",
				r.storeKey(i), err)
		}
		_, err = _conn.Do("ZADD", "updated_"+i.GetNamespace(),
			v.GetUpdated(), r.storeKey(i))
		if err != nil {
			return fmt.Errorf("error updating updated time index %s: %v",
				r.storeKey(i), err)
		}
	}

	if msg, ok := i.(proto.Message); ok {
		val, err := proto.Marshal(msg)
		if err != nil {
			return fmt.Errorf("error marshalling key %s: %v", r.storeKey(i), err)
		}

		_, err = _conn.Do("SET", r.storeKey(i), val)
		if err != nil {
			v := string(val)
			if len(v) > 15 {
				v = v[0:12] + "..."
			}
			return fmt.Errorf("error setting key %s to %s: %v", r.storeKey(i), v, err)
		}
		return nil
	}

	return fmt.Errorf("unsupported object type: %v", i)
}

func (r *redisHandler) Delete(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	_, err := _conn.Do("DEL", r.storeKey(i))
	if err != nil {
		return fmt.Errorf("error deleting key %s: %v", r.storeKey(i), err)
	}
	return err
}

func (r *redisHandler) Read(i item.Item) error {
	_conn := r.pool.Get()
	defer _conn.Close()

	val, err := redis.Bytes(_conn.Do("GET", r.storeKey(i)))
	if err != nil {
		return fmt.Errorf("error getting key %s: %v", r.storeKey(i), err)
	}

	if msg, ok := i.(proto.Message); ok {
		err = proto.Unmarshal(val, msg)
		if err != nil {
			v := string(val)
			if len(v) > 15 {
				v = v[0:12] + "..."
			}
			return fmt.Errorf("error unmarshalling data %s to %s: %v", v, r.storeKey(i), err)
		}
		return nil
	}

	return fmt.Errorf("unsupported object type: %v", i)
}

func getKeys(conn redis.Conn, o item.ListOpt,
	ns string, errc chan error, comm chan string) (count int) {
	count = 0
	handleArr := func(k []string) bool {
		for _, v := range k {
			log.Debugf("Sending %s", v)
			comm <- v
			count++
			if int64(count) == o.Limit {
				log.Debugf("Sent %d items on channel", count)
				return true
			}

		}
		return true
	}
	switch o.Sort {
	case item.SortNatural:
		log.Debugf("Natural sort")
		pattern := ns + "_*"

		arr, err := redis.Values(conn.Do("KEYS", pattern))
		if err != nil {
			errc <- err
			return
		}
		keys, _ := redis.Strings(arr, nil)
		if handleArr(keys) {
			return
		}
	case item.SortCreatedAsc:
		fallthrough
	case item.SortCreatedDesc:
		version := "-inf"
		if o.Version != 0 {
			version = strconv.FormatInt(o.Version, 10)
		}
		log.Infof("Using version %s", version)
		arr, err := redis.Strings(conn.Do("ZRANGEBYSCORE",
			"created_"+ns, version, "+inf"))
		if err != nil {
			errc <- err
			return
		}
		_ = handleArr(arr)
	case item.SortUpdatedAsc:
		fallthrough
	case item.SortUpdatedDesc:
		version := "-inf"
		if o.Version != 0 {
			version = strconv.FormatInt(o.Version, 10)
		}
		arr, err := redis.Strings(conn.Do("ZRANGEBYSCORE",
			"updated_"+ns, version, "+inf"))
		if err != nil {
			errc <- err
			return
		}
		_ = handleArr(arr)
	}
	return
}

func (r *redisHandler) Count(l item.Item) (int, error) {
	_conn := r.pool.Get()
	defer _conn.Close()
	pattern := l.GetNamespace() + "_*"
	arr, err := redis.Values(_conn.Do("KEYS", pattern))
	if err != nil {
		return 0, err
	}
	keys, _ := redis.Strings(arr, nil)
	return len(keys), nil
}

func (r *redisHandler) List(l item.Items, o item.ListOpt) (int, error) {

	if int64(len(l)) < o.Limit {
		return 0, fmt.Errorf("error insufficient items in array to unmarshal required %d got %d",
			o.Limit, len(l))
	}

	comm := make(chan string, o.Limit)
	errc := make(chan error, 1)

	ctx, cancel := context.WithCancel(context.Background())
	count := 0

	wg := sync.WaitGroup{}

	wg.Add(1)
	go func() {
		conn := r.pool.Get()
		defer conn.Close()
		defer wg.Done()
		defer cancel()
		count = getKeys(conn, o, l[0].GetNamespace(), errc, comm)
	}()

	idx := 0
	var retErr error
	wg.Add(1)
	go func() {
		conn := r.pool.Get()
		defer conn.Close()
		defer wg.Done()
		for {
			select {
			case _key := <-comm:
				log.Debugf("Got key %s", _key)
				val, err := redis.Bytes(conn.Do("GET", _key))
				if err == nil {
					if msg, ok := l[idx].(proto.Message); ok {
						err = proto.Unmarshal(val, msg)
					} else {
						err = fmt.Errorf("unsupported object type: %v", l[idx])
					}
				}
				if err != nil {
					retErr = err
					return
				}
				idx++
			case e := <-errc:
				retErr = e
				return
			case <-ctx.Done():
				if idx == count {
					log.Debugf("Done")
					return
				}
				log.Debugf("Have to drain requests first idx %d count %d", idx, count)
			}
		}
	}()

	wg.Wait()

	return idx, retErr
}
