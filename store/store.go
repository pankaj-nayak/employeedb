package store

import (
	"errors"

	configurator "gitlab.com/employeedb/configurator/pb"
	"gitlab.com/employeedb/store/handlers/redis"
	"gitlab.com/employeedb/store/item"
)

const (
	POSTGRES     Handler = "postgres"
	REDIS        Handler = "redis"
	DUMB_DB      Handler = "dumb_db"
	OS_FILESTORE Handler = "os_filestore"
	BOLT_DB      Handler = "boltdb"
)

type (
	Handler string

	Store interface {
		Create(item.Item) error
		Update(item.Item) error
		Delete(item.Item) error
		Read(item.Item) error
		List(item.Items, item.ListOpt) (int, error)
		Count(item.Item) (int, error)
	}
)

func NewStore(conf *configurator.DBConfig) (Store, error) {
	switch Handler(conf.Handler) {
	case REDIS:
		return redis.NewRedisStore(conf)
	default:
		break
	}

	return nil, errors.New("Invalid handler")
}
