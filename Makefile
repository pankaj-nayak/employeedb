#include Makefile.docker

BUILD_DIR  := $(PWD)
PROTO_DIRS := auth employees common configurator  
SVC_DIRS   := auth employees 

PROTOC := $(shell which protoc)

GOOGLE_APIS := $(GOPATH)/src/github.com/grpc-ecosystem/grpc-gateway/third_party/googleapis
#GOOGLE_APIS := $(GOPATH)/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.12.1/third_party/googleapis

INCLUDES := -I$(GOOGLE_APIS)

MSUITE	:= /Users/pankajnayak/go/src/ut-server/go-msuite

GRPC_PLUGIN := --go_out=plugins=grpc,paths=source_relative:.
GATEWAY_PLUGIN := --grpc-gateway_out=logtostderr=true,paths=source_relative:.
#GATEWAY_PLUGIN := --grpc-gateway_out=logtostderr=true,repeated_path_param_separator=ssv:.
SWAGGER_PLUGIN := --swagger_out=logtostderr=true:$(BUILD_DIR)/swagger
DART_PLUGIN := --dart_out=grpc:$(BUILD_DIR)/dart
PROTO_PATH := --proto_path=$(BUILD_DIR):$(MSUITE):.
PROTO_SFX := .proto

proto:
	@echo ""; echo "";
	@echo "===== Make target proto =====";
	@for dir in $(PROTO_DIRS); do \
		cd $(BUILD_DIR)/$$dir/pb; \
		echo "Building gRPC service $$dir"; \
      $(PROTOC) $(INCLUDES) $(GRPC_PLUGIN) $(PROTO_PATH) $$dir$(PROTO_SFX); \
	done
	@echo "===== Done target proto =====";

gw:
	@echo ""; echo "";
	@echo "===== Make target gw =====";
	@for dir in $(PROTO_DIRS); do \
		cd $(BUILD_DIR)/$$dir/pb; \
		echo "Building gRPC gateway for service $$dir"; \
      $(PROTOC) $(INCLUDES) $(GATEWAY_PLUGIN) $(PROTO_PATH) $$dir$(PROTO_SFX); \
	done
	@echo "===== Done target gw =====";

.PHONY: dart

dart:
	@echo ""; echo "";
	@echo "===== Make target dart =====";
	@for dir in $(PROTO_DIRS); do \
		cd $(BUILD_DIR)/$$dir/pb; \
		echo "Building dart definitions for service $$dir"; \
		$(PROTOC) $(INCLUDES) $(DART_PLUGIN) $(PROTO_PATH) $$dir$(PROTO_SFX); \
	done
	@echo "===== Done target dart =====";
	@echo "===== Editing Dart includes =====";
	@for filename in $(BUILD_DIR)/dart/*.dart; do \
     awk '{gsub("[a-zA-Z]*/pb/", "");print}' $$filename > tmp && mv tmp $$filename; \
	done

services:
	@echo ""; echo "";
	@echo "===== Make target services =====";
	@for dir in $(SVC_DIRS); do \
    		cd $(BUILD_DIR)/$$dir/rpc-service; \
    		echo "Building service $$dir"; \
    		go build -i; \
    done
	@echo "===== Done target services =====";

clean:
	@echo ""; echo "";
	@echo "===== Make target clean =====";
	@for dir in $(SVC_DIRS); do \
      echo "Removing executable $$dir"; \
      rm -if $(BUILD_DIR)/$$dir/$$dir; \
    done
	@for dir in $(PROTO_DIRS); do \
	   echo "Removing generated proto files in $$dir"; \
    	rm -if $(BUILD_DIR)/$$dir/pb/*.pb.go; \
    	rm -if $(BUILD_DIR)/$$dir/pb/*.gw.go; \
    	rm -if $(BUILD_DIR)/$$dir/pb/*.swagger.json; \
    done
	@echo "===== Done target clean =====";

all: clean proto gw services dart

tests:
	@cd $(BUILD_DIR)/_test; \
	(./setup.sh); \
	(go test -run /phase=1 -v); \

default: all
