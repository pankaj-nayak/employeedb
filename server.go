package main

import (
	"context"
	"flag"
	"log"
	"time"

	logger "github.com/ipfs/go-log"

	authhttp "gitlab.com/employeedb/auth/http-service"
	authSvc "gitlab.com/employeedb/auth/rpc-service"
	"gitlab.com/employeedb/configurator/config_service"
	Configurator "gitlab.com/employeedb/configurator/pb"
	employeeshttp "gitlab.com/employeedb/employees/http-service"
	employeesSvc "gitlab.com/employeedb/employees/rpc-service"
)

var defaultConf = &Configurator.Configurations{
	SvcName: "go-msuite",
	Version: "1.0.0",
	Created: time.Now().Unix(),
	Updated: time.Now().Unix(),
	ConfigValues: []*Configurator.Config{
		{
			ConfType: Configurator.ConfigType_GRPC_SERVER_CONFIG,
			Type: &Configurator.Config_Server{
				Server: &Configurator.GrpcServerConfig{
					SvcName: "go-msuite",
					Port:    10000,
					UseJwt:  true,
					// UseValidator: true,
					LogLevel:    12,
					UseTracing:  true,
					TracingHost: "localhost:6831",
					Created:     time.Now().Unix(),
					Updated:     time.Now().Unix(),
					SubConfs: []*Configurator.Config{
						{
							ConfType: Configurator.ConfigType_DB_CONFIG,
							Type: &Configurator.Config_Db{
								Db: &Configurator.DBConfig{
									Handler: "redis",
									Hostname: "localhost",
									//Hostname: "redis",
									Port:     6379,
									Created:  time.Now().Unix(),
									Updated:  time.Now().Unix(),
								},
							},
						},
					},
				},
			},
		},
		{
			ConfType: Configurator.ConfigType_PROXY_ENDPOINT_CONFIG,
			Type: &Configurator.Config_Ep{
				Ep: &Configurator.ProxyEndpointConfig{
					SvcName: "go-msuite",
					Port:    8081,
					Created: time.Now().Unix(),
					Updated: time.Now().Unix(),
					Endpoints: []*Configurator.ProxyEndpointConfig_Endpoint{
						{
							Hostname: "localhost:10000",
							SvcName:  "employees",
						},
						{
							Hostname: "localhost:10000",
							SvcName:  "auth",
						},
					},
				},
			},
		},
	},
}

func main() {
	flag.Parse()

	logger.SetLogLevel("*", "Info")

	trainrSvc := config_service.NewService(nil, authSvc.InitFn, authhttp.InitFn, employeesSvc.InitFn, employeeshttp.InitFn)

	trainrSvc.UpdateConfig(defaultConf)
retry:
	ctx, cancel := context.WithCancel(context.Background())
	err := trainrSvc.Start(ctx)

	if err != nil {
		log.Printf("Trainer service stopped. Result:%s\n", err.Error())
		cancel()
	}
	time.Sleep(time.Second * 15)
	log.Println("Trying to restart Trainer Service.")
	goto retry
}
