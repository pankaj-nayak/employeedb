package rpc_service

import (
	"errors"
	"sync/atomic"

	logger "github.com/ipfs/go-log"
	app_errors "gitlab.com/employeedb/app-errors"
	Messages "gitlab.com/employeedb/common/pb"
	"gitlab.com/employeedb/configurator/config_service/grpc_server"
	Configurator "gitlab.com/employeedb/configurator/pb"
	"gitlab.com/employeedb/store"
	"gitlab.com/employeedb/store/item"
	"golang.org/x/net/context"
)

var log = logger.Logger("configurator")

var (
	req_id int64 = 1
)

func getNextId() int64 {
	return atomic.AddInt64(&req_id, 1)
}

const (
	CONF_KEY_PFX  = "config"
	CONFS_KEY_PFX = "configs"
)

type configurator struct {
	baseSrv grpc_server.BaseGrpcService
	db      store.Store
}

type configObj struct {
	*Configurator.Config
}

func (u *configObj) GetNamespace() string {
	return "configurator" + "_" + CONF_KEY_PFX
}

func getConfigObj(u Configurator.ConfigType, v string) *configObj {
	switch u {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Server{
					Server: &Configurator.GrpcServerConfig{
						SvcName: v,
					},
				},
			},
		}
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Ep{
					Ep: &Configurator.ProxyEndpointConfig{
						SvcName: v,
					},
				},
			},
		}
	case Configurator.ConfigType_DB_CONFIG:
		return &configObj{
			Config: &Configurator.Config{
				ConfType: u, Type: &Configurator.Config_Db{
					Db: &Configurator.DBConfig{
						Handler: v,
					},
				},
			},
		}
	default:
		panic("Config type unidentified")
	}
}

func (u *configObj) GetId() string {
	var a, b string
	a = u.ConfType.String()
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		b = u.GetServer().GetSvcName()
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		b = u.GetEp().GetSvcName()
	case Configurator.ConfigType_DB_CONFIG:
		b = u.GetDb().GetHandler()
	default:
		panic("Config type unidentified")
	}
	return a + "_" + b
}

func (u *configObj) GetUpdated() int64 {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return u.GetServer().GetUpdated()
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		return u.GetEp().GetUpdated()
	case Configurator.ConfigType_DB_CONFIG:
		return u.GetDb().GetUpdated()
	}
	panic("Config type unidentified")
	return 0
}

func (u *configObj) GetCreated() int64 {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		return u.GetServer().GetCreated()
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		return u.GetEp().GetCreated()
	case Configurator.ConfigType_DB_CONFIG:
		return u.GetDb().GetCreated()
	}
	panic("Config type unidentified")
	return 0
}

func (u *configObj) SetUpdated(ts int64) {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		u.GetServer().Updated = ts
		return
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		u.GetEp().Updated = ts
		return
	case Configurator.ConfigType_DB_CONFIG:
		u.GetDb().Updated = ts
		return
	}
	panic("Config type unidentified")
}

func (u *configObj) SetCreated(ts int64) {
	switch u.ConfType {
	case Configurator.ConfigType_GRPC_SERVER_CONFIG:
		u.GetServer().Created = ts
		return
	case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
		u.GetEp().Created = ts
		return
	case Configurator.ConfigType_DB_CONFIG:
		u.GetDb().Created = ts
		return
	}
	panic("Config type unidentified")
}

type configsObj struct {
	*Configurator.Configurations
}

func (u *configsObj) GetId() string { return u.SvcName + "_" + u.Version }

func (u *configsObj) GetNamespace() string { return "configurator" + "_" + CONFS_KEY_PFX }

func (l *configsObj) SetCreated(i int64) { l.Created = i }

func (l *configsObj) SetUpdated(i int64) { l.Updated = i }

type configsList []*configsObj

var InitFn grpc_server.RPCInitFn = Init

var DB = "redis"
var LOCKER = "zookeeper"

func Init(baseSrv grpc_server.BaseGrpcService) error {

	if baseSrv.GetDb(DB) == nil {
		return errors.New("Incomplete config for configurator")
	}
	Configurator.RegisterConfiguratorServer(baseSrv.GetRPCServer(), &configurator{
		db:      baseSrv.GetDb(DB),
		baseSrv: baseSrv,
	})
	return nil
}

func (s *configurator) getConfigWithLock(v *Configurator.ConfigReq) (*configObj, error) {

	conf := getConfigObj(v.ConfType, v.Handler)

	err := s.db.Read(conf)
	if err != nil {
		log.Errorf("Failed storing config in DB. Err:%s Conf:%v", err, conf)
		return nil, err
	} else {
		log.Infof("Got stored config %v", conf)
	}
	return conf, nil
}

func (s *configurator) GetConfigurations(c context.Context,
	req *Configurator.Request) (retConfigs *Configurator.Configurations, retErr error) {

	confs := &configsObj{
		Configurations: &Configurator.Configurations{
			SvcName: req.SvcName,
			Version: req.Version,
		},
	}
	newEntry := false
	err := s.db.Read(confs)
	if err != nil {
		newEntry = true
	}
	var last_update int64

	newConfs := new(Configurator.Configurations)
	newConfs.ConfigValues = make([]*Configurator.Config, len(req.Configs))
	for idx, v := range req.Configs {
		u_conf, err := s.getConfigWithLock(v)
		if err != nil {
			retErr = err
			return
		}
		if u_conf.GetUpdated() > last_update {
			log.Infof("CONF TS:%d", u_conf.GetUpdated())
			last_update = u_conf.GetUpdated()
		}
		newConfs.ConfigValues[idx] = u_conf.Config
		if u_conf.ConfType == Configurator.ConfigType_GRPC_SERVER_CONFIG {
			srv_conf := u_conf.GetServer()
			srv_conf.SubConfs = make([]*Configurator.Config, len(v.SubConfs))
			for idx2, v2 := range v.SubConfs {
				sub_conf, err := s.getConfigWithLock(v2)
				if err != nil {
					retErr = err
					return
				}
				srv_conf.SubConfs[idx2] = sub_conf.Config
				if sub_conf.GetUpdated() > last_update {
					last_update = sub_conf.GetUpdated()
					log.Infof("SUB CONF TS:%d", sub_conf.GetUpdated())
				}
			}
		}
	}
	if confs.Updated < last_update {
		confs.Configurations = newConfs
	}
	if newEntry {
		err = s.db.Create(confs)
	} else {
		err = s.db.Update(confs)
	}
	if err != nil {
		retErr = app_errors.ErrInternal("Failed to store configurations.")
		log.Errorf("Failed storing config in DB. Err:%s Conf:%v", err, confs)
		return
	}
	retConfigs = confs.Configurations
	return
}

func (s *configurator) GetServices(c context.Context,
	emp *Messages.EmptyMessage) (retConfs *Configurator.ConfigurationsList, retErr error) {

	no_of_services := 20

	opt := item.ListOpt{
		Page:  0,
		Limit: int64(no_of_services),
	}
	confs := make([]item.Item, no_of_services)
	for i := range confs {
		confs[i] = new(configsObj)
	}
	results, err := s.db.List(confs, opt)
	if err != nil {
		retErr = app_errors.ErrInternal("Failed to list configurations.")
		log.Errorf("Failed listing configs in DB. Err:%s", err)
		return
	}
	retConfs = &Configurator.ConfigurationsList{Conf: make([]*Configurator.Configurations, results)}
	for i := 0; i < results; i++ {
		if c, ok := confs[i].(*configsObj); ok {
			retConfs.Conf[i] = c.Configurations
		}
	}
	return
}

func (s *configurator) UpdateConfig(c context.Context,
	upd *Configurator.Config) (retConf *Configurator.Config, retErr error) {

	conf := &configObj{Config: upd}
	err := s.db.Update(conf)
	if err != nil {
		err = app_errors.ErrInternal("Failed to store config.")
		log.Errorf("Failed storing config in DB. Err:%s Conf:%v", err, upd)
		return nil, err
	}
	retConf = upd
	return
}
