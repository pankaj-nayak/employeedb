package grpc_gateway

import (
	"net/http"
	"strconv"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	logger "github.com/ipfs/go-log"
	"gitlab.com/employeedb/common/objects"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var log = logger.Logger("grpc_gateway")

type HTTPInitFn func(context.Context, *runtime.ServeMux, *objects.ProxyConf, []grpc.DialOption) error

type GatewayServer interface {
	Start(context.Context, ...HTTPInitFn) error
}

type gatewayServer struct {
	*objects.ProxyConf
}

func NewGatewayService(conf *objects.ProxyConf) GatewayServer { return &gatewayServer{ProxyConf: conf} }

func (gw *gatewayServer) Start(ctx context.Context, gwInit ...HTTPInitFn) error {
	mux := runtime.NewServeMux(runtime.WithMarshalerOption(runtime.MIMEWildcard,
		&runtime.JSONPb{OrigName: true, EmitDefaults: true}))

	opts := []grpc.DialOption{grpc.WithInsecure()}
	for _, httpInit := range gwInit {
		err := httpInit(ctx, mux, gw.ProxyConf, opts)
		if err != nil {
			log.Info("Error registering grpc-gateway:" + err.Error())
			return err
		}
	}

	log.Infof("Registered grpc_gateway with %v. Running on port %d", gw.SvcName, gw.Port)
	var err error
	httpServer := &http.Server{Addr: ":" + strconv.Itoa(int(gw.Port)), Handler: mux}
	started := false
	for {
		select {
		case <-ctx.Done():
			log.Info("Shutting down HTTP server.")
			return httpServer.Shutdown(nil)
		default:
			if !started {
				go func() {
					err = httpServer.ListenAndServe()
				}()
				started = true
			}
		}
	}
	return err
}
