package grpc_gateway

import (
	"testing"
	"time"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/pkg/errors"
	"gitlab.com/employeedb/common/objects"
	Configurator "gitlab.com/employeedb/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

func TestGatewayServer_Start(t *testing.T) {
	var errInitFn HTTPInitFn = func(context.Context, *runtime.ServeMux, *objects.ProxyConf, []grpc.DialOption) error {
		return errors.New("Dummy Error")
	}

	var initFn HTTPInitFn = func(context.Context, *runtime.ServeMux, *objects.ProxyConf, []grpc.DialOption) error {
		return nil
	}

	gwSvc := &gatewayServer{}

	err := gwSvc.Start(context.Background(), errInitFn)
	if err == nil {
		t.Fatal("Expected error on start.")
	}

	gwSvc = &gatewayServer{
		ProxyConf: &objects.ProxyConf{
			ProxyEndpointConfig: &Configurator.ProxyEndpointConfig{
				SvcName: "TestGatewayServer",
				Port:    1000,
			},
		},
	}

	done := make(chan bool, 1)
	ctx, cancel := context.WithCancel(context.Background())
	go func(_ch chan bool) {
		err := gwSvc.Start(ctx, initFn)
		if err != nil {
			_ch <- false
		} else {
			_ch <- true
		}
	}(done)

	// Issue stop
	cancel()

	start := time.Now()
	for {
		stop := false
		select {
		case done_sig := <-done:
			if !done_sig {
				t.Fatal("Service should not have returned error on external stop.")
			}
			stop = true
			break
		default:
			if time.Since(start) > time.Minute {
				t.Fatal("Service took too long to stop.")
			}
		}
		if stop {
			break
		}
	}
}
