package config_service

import (
	"errors"
	"sync"
	"time"

	logger "github.com/ipfs/go-log"
	"gitlab.com/employeedb/common/objects"
	"gitlab.com/employeedb/configurator/config_service/grpc_gateway"
	"gitlab.com/employeedb/configurator/config_service/grpc_server"
	Configurator "gitlab.com/employeedb/configurator/pb"
	"golang.org/x/net/context"
)

var log = logger.Logger("Service::Main")

type Service interface {
	GetArgs() *Configurator.Request
	GetConfigLastUpdated() int64
	UpdateConfig(c *Configurator.Configurations)
	Start(ctx context.Context) error
}

type service struct {
	args     *Configurator.Request
	conf_mtx sync.Mutex
	conf     *Configurator.Configurations
	init_fns []interface{}
}

func NewService(conf *Configurator.Request, inits ...interface{}) Service {
	return &service{
		args:     conf,
		init_fns: inits,
	}
}

func (s *service) GetArgs() *Configurator.Request {
	return s.args
}

func (s *service) GetConfigLastUpdated() int64 {
	s.conf_mtx.Lock()
	defer s.conf_mtx.Unlock()
	return s.conf.Updated
}

func (s *service) UpdateConfig(c *Configurator.Configurations) {
	s.conf_mtx.Lock()
	defer s.conf_mtx.Unlock()
	s.conf = c
}

func (s *service) Start(ctx context.Context) (ret_err error) {

	wg := &sync.WaitGroup{}

	errc := make(chan error, 2)
	defer close(errc)

	cctx, cancel := context.WithCancel(ctx)
	go func() {
		<-ctx.Done()
		log.Infof("Parent requested stop.")
		cancel()
	}()

	wg.Add(1)
	go func() {
		defer wg.Done()
		for {
			select {
			case ret_err = <-errc:
				log.Errorf("One of the services returned error %v. Quitting.", ret_err)
				cancel()
				return
			case <-cctx.Done():
				log.Info("Stop requested. Returning.")
				return
			default:
				time.Sleep(time.Millisecond * 5000)
			}
		}
	}()

	// Map to make sure same port is not used.Currently multiple services
	// are allowed to run on independent ports.
	started := make(map[int32]bool)
	for idx := range s.conf.ConfigValues {
		switch s.conf.ConfigValues[idx].ConfType {
		case Configurator.ConfigType_GRPC_SERVER_CONFIG:
			server_conf := &objects.GrpcServerConf{
				GrpcServerConfig: s.conf.ConfigValues[idx].GetServer()}

			if already_started, ok := started[server_conf.GetPort()]; ok {
				if already_started {
					log.Error("Not allowed to start service on same port")
					errc <- errors.New("Invalid port no.")
				}
			} else {
				started[server_conf.GetPort()] = true
			}

			initFns := []grpc_server.RPCInitFn{}

			for i := range s.init_fns {
				if fn, ok := s.init_fns[i].(grpc_server.RPCInitFn); ok {
					log.Info("Found INIT fn")
					initFns = append(initFns, fn)
				}
			}

			if len(initFns) > 0 {
				wg.Add(1)
				go func(conf *objects.GrpcServerConf) {
					defer wg.Done()
					rpc_srv := grpc_server.NewGrpcService(conf)
					err := rpc_srv.Start(cctx, initFns...)
					if err != nil {
						log.Error("RPC service returned ERR:" + err.Error())
						errc <- err
					}
				}(server_conf)
			} else {
				errc <- errors.New("GRPC Init function not provided.")
			}
		case Configurator.ConfigType_PROXY_ENDPOINT_CONFIG:
			proxy_conf := &objects.ProxyConf{ProxyEndpointConfig: s.conf.ConfigValues[idx].GetEp()}

			if already_started, ok := started[proxy_conf.GetPort()]; ok {
				if already_started {
					log.Error("Not allowed to start service on same port")
					errc <- errors.New("Invalid port no.")
				}
			} else {
				started[proxy_conf.GetPort()] = true
			}

			initFns := []grpc_gateway.HTTPInitFn{}

			for i := range s.init_fns {
				if fn, ok := s.init_fns[i].(grpc_gateway.HTTPInitFn); ok {
					log.Info("Found HTTP INIT fn")
					initFns = append(initFns, fn)
				}
			}

			if len(initFns) > 0 {
				wg.Add(1)
				go func(conf *objects.ProxyConf) {
					defer wg.Done()
					gwSvc := grpc_gateway.NewGatewayService(conf)
					err := gwSvc.Start(cctx, initFns...)
					if err != nil {
						log.Error("HTTP service returned ERR:" + err.Error())
						errc <- err
					}
				}(proxy_conf)
			} else {
				errc <- errors.New("GRPC Gateway Init function not provided.")
			}
		}
	}

	wg.Wait()
	log.Info("Services stopped. Returning.")
	return
}
