package grpc_server

import (
	"fmt"

	logger "github.com/ipfs/go-log"

	"gitlab.com/employeedb/common/objects"

	"gitlab.com/employeedb/store"
	"golang.org/x/net/context"
	"google.golang.org/grpc"

	"net"
)

var log = logger.Logger("grpc_server")

type RPCInitFn func(BaseGrpcService) error

type GrpcServer interface {
	Start(context.Context, ...RPCInitFn) error
}

type grpcServer struct {
	*objects.GrpcServerConf
}

func NewGrpcService(conf *objects.GrpcServerConf) GrpcServer {
	return &grpcServer{GrpcServerConf: conf}
}

func (s *grpcServer) Start(ctx context.Context, rpcInits ...RPCInitFn) error {

	if s.GetUseJwt() {
		log.Info("Using default auth function for JWT validation.")
	} else {
		log.Info("Disabling JWT validation.")
	}

	if s.GetUseRecovery() {
		log.Info("Using default recovery handler for grpc recovery.")
	} else {
		log.Info("Disabling grpc recovery.")
	}

	if s.GetUseValidator() {
		log.Info("Enabling grpc validator.")
	} else {
		log.Info("Disabling grpc validator.")
	}

	opts, err := s.GetServerOpts()
	if err != nil {
		log.Info("Error getting grpc server options:" + err.Error())
		return err
	}

	log.Infof("Using server options %v", opts)

	port := fmt.Sprintf(":%d", s.GetPort())
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Info("Error starting TCP listener:" + err.Error())
		return err
	}
	log.Infof("Started TCP listener on [%s]\n", port)
	defer lis.Close()

	grpcServer := grpc.NewServer(opts...)

	svcBase, err := newGrpcServiceBase(ctx, s.GrpcServerConf, grpcServer)
	if err != nil {
		return err
	}
	for _, rpcInit := range rpcInits {
		err = rpcInit(svcBase)
		if err != nil {
			log.Info("Error getting grpc server options:" + err.Error())
			return err
		}
		log.Info("Registered new RPC service")
	}
	log.Info("Initialized RPC service. Starting.")

	started := false
	for {
		select {
		case <-ctx.Done():
			log.Info("Shutting down RPC server.")
			grpcServer.Stop()
			return nil
		default:
			if !started {
				go func() {
					err = grpcServer.Serve(lis)
				}()
				started = true
			}
		}
	}
	return err
}

type BaseGrpcService interface {
	Background() context.Context
	GetRPCServer() *grpc.Server
	GetConf() *objects.GrpcServerConf
	GetDb(string) store.Store
}

type grpcServiceBase struct {
	*objects.GrpcServerConf
	baseCtx context.Context
	srv     *grpc.Server
	stores  map[string]store.Store
}

func newGrpcServiceBase(ctx context.Context, c *objects.GrpcServerConf,
	srv *grpc.Server) (*grpcServiceBase, error) {

	stores := make(map[string]store.Store)
	for _, conf := range c.SubConfs {
		if dbConf := conf.GetDb(); dbConf != nil {
			dbp, err := store.NewStore(dbConf)
			if err != nil {
				log.Errorf("Failed initializing store Err:%s", err.Error())
				return nil, err
			}
			stores[dbConf.Handler] = dbp
			log.Infof("Created new %s store", dbConf.Handler)
		}
	}
	s := &grpcServiceBase{
		GrpcServerConf: c,
		baseCtx:        ctx,
		srv:            srv,
		stores:         stores,
	}
	return s, nil
}

func (s *grpcServiceBase) Background() context.Context {
	return s.baseCtx
}

func (s *grpcServiceBase) GetRPCServer() *grpc.Server {
	return s.srv
}

func (s *grpcServiceBase) GetConf() *objects.GrpcServerConf {
	return s.GrpcServerConf
}

func (s *grpcServiceBase) GetDb(handler string) store.Store {
	if s, ok := s.stores[handler]; ok {
		return s
	}
	return nil
}
