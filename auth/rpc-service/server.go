package rpc_service

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	logger "github.com/ipfs/go-log"
	app_errors "gitlab.com/employeedb/app-errors"
	spec "gitlab.com/employeedb/auth/pb"
	"gitlab.com/employeedb/common/objects/utils"
	msgs "gitlab.com/employeedb/common/pb"
	"gitlab.com/employeedb/configurator/config_service/grpc_server"
	"gitlab.com/employeedb/store"
	"golang.org/x/crypto/bcrypt"
	"golang.org/x/net/context"
)

var log = logger.Logger("auth")

type verifiedUser struct {
	*spec.VerifiedUser
}

func (u *verifiedUser) GetNamespace() string {
	return "auth" + "_" + "VerifiedUser"
}

func (u *verifiedUser) GetId() string {
	return u.GetRole() + "_" + u.GetUsername()
}

func (u *verifiedUser) SetId(id string) { u.UserId = id }

type authServer struct {
	baseSrv grpc_server.BaseGrpcService
	dbP     store.Store
}

var InitFn grpc_server.RPCInitFn = Init

var DB = "redis"

func Init(base grpc_server.BaseGrpcService) error {
	if base.GetDb(DB) == nil {
		return errors.New("Incomplete configuration for Auth")
	}
	spec.RegisterAuthServer(base.GetRPCServer(), &authServer{
		dbP:     base.GetDb(DB),
		baseSrv: base,
	})
	log.Info("Auth service registered")
	return nil
}

func (s *authServer) verify_fn() func(token *jwt.Token) (interface{}, error) {
	return func(token *jwt.Token) (interface{}, error) {
		return s.baseSrv.GetConf().PubKey, nil
	}
}

func (s *authServer) createAdminUser(c context.Context,
	verify *spec.AuthCredentials) (retEmp *msgs.EmptyMessage, retErr error) {
	log.Infof("CreateAdminUser : %v", verify)

	hashedPass, err := bcrypt.GenerateFromPassword(
		[]byte(verify.Password), bcrypt.DefaultCost)
	if err != nil {
		retErr = app_errors.ErrInternal("Internal server error.")
		log.Errorf("Failed to generate password hash. SecErr:%s", err.Error())
		return
	}

	verifiedUsr := &verifiedUser{
		&spec.VerifiedUser{
			Username: verify.Username,
			Password: hashedPass,
			Role:     verify.Role,
		},
	}
	log.Infof("verifiedUsrObj: %v", verifiedUsr)
	err = s.dbP.Create(verifiedUsr)
	if err != nil {
		retErr = app_errors.ErrInternal("Failed creating user entry.")
		log.Errorf("Failed to create verified user SecErr:%s", err.Error())
		return
	}
	retEmp = &msgs.EmptyMessage{}
	return
}

func (s *authServer) Login(c context.Context,
	creds *spec.AuthCredentials) (result *spec.AuthResult, retErr error) {

	authCred := &spec.AuthCredentials{
		Username: "admin",
		Password: "secret",
		Role:     "admin",
	}

	_, err := s.createAdminUser(c, authCred)

	if err != nil {
		retErr = app_errors.ErrPermissionDenied("Unable to create admin user ")
		log.Errorf("Unable to create Admin user:%s", err.Error())
		return

	}
	usrObj := &verifiedUser{
		&spec.VerifiedUser{
			Username: creds.Username,
			Role:     creds.Role,
		},
	}

	err = s.dbP.Read(usrObj)
	if err != nil {
		retErr = app_errors.ErrPermissionDenied("Username does not exist. Please signup first.")
		log.Errorf("Username %s does not exist. SecErr:%s", usrObj.GetId(), err.Error())
		return
	}
	if err = bcrypt.CompareHashAndPassword(usrObj.Password, []byte(creds.Password)); err != nil {
		retErr = app_errors.ErrPermissionDenied("Password incorrect.")
		log.Errorf("Password doesnt match. SecErr:%v", err)
		return
	}
	accTok, refTok, err := utils.CreateTokens(usrObj.UserId, usrObj.Username, usrObj.Role)
	if err != nil {
		retErr = app_errors.ErrInternal("Internal server error.")
		log.Errorf("Failed creating tokens. SecErr:%s", err.Error())
		return
	}
	result = &spec.AuthResult{
		UserId:       usrObj.UserId,
		AccessToken:  accTok,
		RefreshToken: refTok,
	}
	log.Infof("User %s login successful", usrObj.UserId)

	return
}

func (s *authServer) RefreshToken(c context.Context,
	currToken *spec.AuthResult) (result *spec.AuthResult, retErr error) {

	if len(currToken.RefreshToken) == 0 {
		retErr = app_errors.ErrInvalidArg("Token not present.")
		log.Error("Refresh token empty")
		return
	}
	token, err := jwt.ParseWithClaims(currToken.RefreshToken, &utils.AuthClaims{}, s.verify_fn())
	if err != nil {
		retErr = app_errors.ErrUnauthenticated("Invalid refresh token.")
		log.Errorf("Error while parsing claims SecErr:%s", err.Error())
		return
	}
	claims := token.Claims.(*utils.AuthClaims)
	accTok, refTok, err := utils.CreateTokens(claims.UserUuid, claims.Username, claims.LoginType)
	if err != nil {
		retErr = app_errors.ErrInternal("Internal server error.")
		log.Errorf("Failed creating tokens. SecErr:%s", err.Error())
		return
	}
	result = &spec.AuthResult{
		UserId:       claims.UserUuid,
		AccessToken:  accTok,
		RefreshToken: refTok,
	}
	log.Infof("Access token refresh successful for user %s", claims.UserUuid)
	return
}

func (s *authServer) AuthFuncOverride(ctx context.Context, fullMethodName string) (context.Context, error) {
	return ctx, nil
}
