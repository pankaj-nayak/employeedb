package http_service

import (
	"context"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/pkg/errors"
	gw "gitlab.com/employeedb/auth/pb"
	"gitlab.com/employeedb/common/objects"
	"gitlab.com/employeedb/configurator/config_service/grpc_gateway"
	"google.golang.org/grpc"
)

var InitFn grpc_gateway.HTTPInitFn = Init

// Any additional options etc can be supplied here.
func Init(ctx context.Context, mux *runtime.ServeMux, conf *objects.ProxyConf, opts []grpc.DialOption) error {

	c := conf.GetProxyEp("auth")
	if c == nil {
		return errors.New("Auth endpoint absent.")
	}

	return gw.RegisterAuthHandlerFromEndpoint(ctx, mux, c.Hostname, opts)
}
