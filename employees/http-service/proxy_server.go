package http_service

import (
	"context"
	"errors"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"gitlab.com/employeedb/common/objects"
	"gitlab.com/employeedb/configurator/config_service/grpc_gateway"
	gw "gitlab.com/employeedb/employees/pb"
	"google.golang.org/grpc"
)

var InitFn grpc_gateway.HTTPInitFn = Init

// Any additional options etc can be supplied here.
func Init(ctx context.Context, mux *runtime.ServeMux, conf *objects.ProxyConf, opts []grpc.DialOption) error {
	c := conf.GetProxyEp("employees")
	if c == nil {
		c = conf.GetProxyEp("go-msuite")
	}
	if c == nil {
		return errors.New("employees endpoint absent.")
	}

	return gw.RegisterEmployeesHandlerFromEndpoint(ctx, mux, c.Hostname, opts)
}
