///
//  Generated code. Do not modify.
//  source: employees.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class Gender extends $pb.ProtobufEnum {
  static const Gender INVALID = Gender._(0, 'INVALID');
  static const Gender MALE = Gender._(1, 'MALE');
  static const Gender FEMALE = Gender._(2, 'FEMALE');

  static const $core.List<Gender> values = <Gender> [
    INVALID,
    MALE,
    FEMALE,
  ];

  static final $core.Map<$core.int, Gender> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Gender valueOf($core.int value) => _byValue[value];

  const Gender._($core.int v, $core.String n) : super(v, n);
}

class Department extends $pb.ProtobufEnum {
  static const Department INVALID_DEPARTMEN = Department._(0, 'INVALID_DEPARTMEN');
  static const Department DEVELOPMENT = Department._(1, 'DEVELOPMENT');
  static const Department TESTING = Department._(2, 'TESTING');
  static const Department HR = Department._(3, 'HR');
  static const Department MANAGER = Department._(4, 'MANAGER');
  static const Department STAFF = Department._(5, 'STAFF');

  static const $core.List<Department> values = <Department> [
    INVALID_DEPARTMEN,
    DEVELOPMENT,
    TESTING,
    HR,
    MANAGER,
    STAFF,
  ];

  static final $core.Map<$core.int, Department> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Department valueOf($core.int value) => _byValue[value];

  const Department._($core.int v, $core.String n) : super(v, n);
}

class Active extends $pb.ProtobufEnum {
  static const Active NOT_SET = Active._(0, 'NOT_SET');
  static const Active FALSE = Active._(1, 'FALSE');
  static const Active TRUE = Active._(2, 'TRUE');

  static const $core.List<Active> values = <Active> [
    NOT_SET,
    FALSE,
    TRUE,
  ];

  static final $core.Map<$core.int, Active> _byValue = $pb.ProtobufEnum.initByValue(values);
  static Active valueOf($core.int value) => _byValue[value];

  const Active._($core.int v, $core.String n) : super(v, n);
}

