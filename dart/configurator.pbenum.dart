///
//  Generated code. Do not modify.
//  source: configurator.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

// ignore_for_file: UNDEFINED_SHOWN_NAME,UNUSED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class ConfigType extends $pb.ProtobufEnum {
  static const ConfigType RESERVED = ConfigType._(0, 'RESERVED');
  static const ConfigType GRPC_SERVER_CONFIG = ConfigType._(1, 'GRPC_SERVER_CONFIG');
  static const ConfigType PROXY_ENDPOINT_CONFIG = ConfigType._(2, 'PROXY_ENDPOINT_CONFIG');
  static const ConfigType DB_CONFIG = ConfigType._(4, 'DB_CONFIG');

  static const $core.List<ConfigType> values = <ConfigType> [
    RESERVED,
    GRPC_SERVER_CONFIG,
    PROXY_ENDPOINT_CONFIG,
    DB_CONFIG,
  ];

  static final $core.Map<$core.int, ConfigType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static ConfigType valueOf($core.int value) => _byValue[value];

  const ConfigType._($core.int v, $core.String n) : super(v, n);
}

