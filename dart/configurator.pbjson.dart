///
//  Generated code. Do not modify.
//  source: configurator.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const ConfigType$json = const {
  '1': 'ConfigType',
  '2': const [
    const {'1': 'RESERVED', '2': 0},
    const {'1': 'GRPC_SERVER_CONFIG', '2': 1},
    const {'1': 'PROXY_ENDPOINT_CONFIG', '2': 2},
    const {'1': 'DB_CONFIG', '2': 4},
  ],
};

const ConfigReq$json = const {
  '1': 'ConfigReq',
  '2': const [
    const {'1': 'conf_type', '3': 1, '4': 1, '5': 14, '6': '.configurator.ConfigType', '10': 'confType'},
    const {'1': 'handler', '3': 2, '4': 1, '5': 9, '10': 'handler'},
    const {'1': 'sub_confs', '3': 3, '4': 3, '5': 11, '6': '.configurator.ConfigReq', '10': 'subConfs'},
  ],
};

const Config$json = const {
  '1': 'Config',
  '2': const [
    const {'1': 'conf_type', '3': 1, '4': 1, '5': 14, '6': '.configurator.ConfigType', '10': 'confType'},
    const {'1': 'server', '3': 2, '4': 1, '5': 11, '6': '.configurator.GrpcServerConfig', '9': 0, '10': 'server'},
    const {'1': 'ep', '3': 3, '4': 1, '5': 11, '6': '.configurator.ProxyEndpointConfig', '9': 0, '10': 'ep'},
    const {'1': 'db', '3': 4, '4': 1, '5': 11, '6': '.configurator.DBConfig', '9': 0, '10': 'db'},
  ],
  '8': const [
    const {'1': 'Type'},
  ],
};

const ConfigurationsList$json = const {
  '1': 'ConfigurationsList',
  '2': const [
    const {'1': 'conf', '3': 1, '4': 3, '5': 11, '6': '.configurator.Configurations', '10': 'conf'},
  ],
};

const Configurations$json = const {
  '1': 'Configurations',
  '2': const [
    const {'1': 'svc_name', '3': 1, '4': 1, '5': 9, '10': 'svcName'},
    const {'1': 'version', '3': 2, '4': 1, '5': 9, '10': 'version'},
    const {'1': 'config_values', '3': 3, '4': 3, '5': 11, '6': '.configurator.Config', '10': 'configValues'},
    const {'1': 'created', '3': 98, '4': 1, '5': 3, '10': 'created'},
    const {'1': 'updated', '3': 99, '4': 1, '5': 3, '10': 'updated'},
  ],
};

const Request$json = const {
  '1': 'Request',
  '2': const [
    const {'1': 'svc_name', '3': 1, '4': 1, '5': 9, '10': 'svcName'},
    const {'1': 'version', '3': 2, '4': 1, '5': 9, '10': 'version'},
    const {'1': 'configs', '3': 3, '4': 3, '5': 11, '6': '.configurator.ConfigReq', '10': 'configs'},
  ],
};

const GrpcServerConfig$json = const {
  '1': 'GrpcServerConfig',
  '2': const [
    const {'1': 'svc_name', '3': 1, '4': 1, '5': 9, '10': 'svcName'},
    const {'1': 'use_tls', '3': 2, '4': 1, '5': 8, '10': 'useTls'},
    const {'1': 'use_jwt', '3': 3, '4': 1, '5': 8, '10': 'useJwt'},
    const {'1': 'use_validator', '3': 4, '4': 1, '5': 8, '10': 'useValidator'},
    const {'1': 'use_recovery', '3': 5, '4': 1, '5': 8, '10': 'useRecovery'},
    const {'1': 'port', '3': 6, '4': 1, '5': 5, '10': 'port'},
    const {'1': 'log_level', '3': 7, '4': 1, '5': 5, '10': 'logLevel'},
    const {'1': 'cert_file', '3': 8, '4': 1, '5': 9, '10': 'certFile'},
    const {'1': 'key_file', '3': 9, '4': 1, '5': 9, '10': 'keyFile'},
    const {'1': 'pub_key_file', '3': 10, '4': 1, '5': 9, '10': 'pubKeyFile'},
    const {'1': 'priv_key_file', '3': 11, '4': 1, '5': 9, '10': 'privKeyFile'},
    const {'1': 'sub_confs', '3': 12, '4': 3, '5': 11, '6': '.configurator.Config', '10': 'subConfs'},
    const {'1': 'use_tracing', '3': 13, '4': 1, '5': 8, '10': 'useTracing'},
    const {'1': 'tracing_host', '3': 14, '4': 1, '5': 9, '10': 'tracingHost'},
    const {'1': 'created', '3': 98, '4': 1, '5': 3, '10': 'created'},
    const {'1': 'updated', '3': 99, '4': 1, '5': 3, '10': 'updated'},
  ],
};

const ProxyEndpointConfig$json = const {
  '1': 'ProxyEndpointConfig',
  '2': const [
    const {'1': 'endpoints', '3': 1, '4': 3, '5': 11, '6': '.configurator.ProxyEndpointConfig.Endpoint', '10': 'endpoints'},
    const {'1': 'svc_name', '3': 2, '4': 1, '5': 9, '10': 'svcName'},
    const {'1': 'port', '3': 3, '4': 1, '5': 5, '10': 'port'},
    const {'1': 'created', '3': 98, '4': 1, '5': 3, '10': 'created'},
    const {'1': 'updated', '3': 99, '4': 1, '5': 3, '10': 'updated'},
  ],
  '3': const [ProxyEndpointConfig_Endpoint$json],
};

const ProxyEndpointConfig_Endpoint$json = const {
  '1': 'Endpoint',
  '2': const [
    const {'1': 'svc_name', '3': 1, '4': 1, '5': 9, '10': 'svcName'},
    const {'1': 'hostname', '3': 2, '4': 1, '5': 9, '10': 'hostname'},
  ],
};

const DBConfig$json = const {
  '1': 'DBConfig',
  '2': const [
    const {'1': 'handler', '3': 1, '4': 1, '5': 9, '10': 'handler'},
    const {'1': 'hostname', '3': 2, '4': 1, '5': 9, '10': 'hostname'},
    const {'1': 'port', '3': 3, '4': 1, '5': 5, '10': 'port'},
    const {'1': 'username', '3': 4, '4': 1, '5': 9, '10': 'username'},
    const {'1': 'password', '3': 5, '4': 1, '5': 9, '10': 'password'},
    const {'1': 'db_name', '3': 6, '4': 1, '5': 9, '10': 'dbName'},
    const {'1': 'db_path', '3': 7, '4': 1, '5': 9, '10': 'dbPath'},
    const {'1': 'created', '3': 98, '4': 1, '5': 3, '10': 'created'},
    const {'1': 'updated', '3': 99, '4': 1, '5': 3, '10': 'updated'},
  ],
};

