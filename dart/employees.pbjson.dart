///
//  Generated code. Do not modify.
//  source: employees.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const Gender$json = const {
  '1': 'Gender',
  '2': const [
    const {'1': 'INVALID', '2': 0},
    const {'1': 'MALE', '2': 1},
    const {'1': 'FEMALE', '2': 2},
  ],
};

const Department$json = const {
  '1': 'Department',
  '2': const [
    const {'1': 'INVALID_DEPARTMEN', '2': 0},
    const {'1': 'DEVELOPMENT', '2': 1},
    const {'1': 'TESTING', '2': 2},
    const {'1': 'HR', '2': 3},
    const {'1': 'MANAGER', '2': 4},
    const {'1': 'STAFF', '2': 5},
  ],
};

const Active$json = const {
  '1': 'Active',
  '2': const [
    const {'1': 'NOT_SET', '2': 0},
    const {'1': 'FALSE', '2': 1},
    const {'1': 'TRUE', '2': 2},
  ],
};

const UUID$json = const {
  '1': 'UUID',
  '2': const [
    const {'1': 'val', '3': 1, '4': 1, '5': 9, '10': 'val'},
  ],
};

const UUIDs$json = const {
  '1': 'UUIDs',
  '2': const [
    const {'1': 'vals', '3': 1, '4': 3, '5': 9, '10': 'vals'},
  ],
};

const Item$json = const {
  '1': 'Item',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'department', '3': 3, '4': 1, '5': 14, '6': '.employees.Department', '10': 'department'},
    const {'1': 'location', '3': 4, '4': 1, '5': 9, '10': 'location'},
    const {'1': 'skills', '3': 5, '4': 3, '5': 9, '10': 'skills'},
    const {'1': 'active', '3': 6, '4': 1, '5': 14, '6': '.employees.Active', '10': 'active'},
  ],
};

const Items$json = const {
  '1': 'Items',
  '2': const [
    const {'1': 'items', '3': 1, '4': 3, '5': 11, '6': '.employees.Item', '10': 'items'},
  ],
};

const RestoreRequest$json = const {
  '1': 'RestoreRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
  ],
};

const ListReq$json = const {
  '1': 'ListReq',
  '2': const [
    const {'1': 'id', '3': 1, '4': 3, '5': 9, '10': 'id'},
    const {'1': 'name', '3': 2, '4': 1, '5': 9, '10': 'name'},
    const {'1': 'department', '3': 3, '4': 1, '5': 14, '6': '.employees.Department', '10': 'department'},
  ],
};

const SearchReq$json = const {
  '1': 'SearchReq',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 9, '10': 'value'},
  ],
};

const DeleteRequest$json = const {
  '1': 'DeleteRequest',
  '2': const [
    const {'1': 'id', '3': 1, '4': 1, '5': 9, '10': 'id'},
    const {'1': 'delete_permanently', '3': 2, '4': 1, '5': 8, '10': 'deletePermanently'},
  ],
};

const Response$json = const {
  '1': 'Response',
  '2': const [
    const {'1': 'value', '3': 1, '4': 1, '5': 8, '10': 'value'},
  ],
};

