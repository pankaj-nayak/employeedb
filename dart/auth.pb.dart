///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

class AuthCredentials extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthCredentials', package: const $pb.PackageName('auth'), createEmptyInstance: create)
    ..aOS(1, 'username')
    ..aOS(2, 'password')
    ..aOS(3, 'role')
    ..hasRequiredFields = false
  ;

  AuthCredentials._() : super();
  factory AuthCredentials() => create();
  factory AuthCredentials.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AuthCredentials.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AuthCredentials clone() => AuthCredentials()..mergeFromMessage(this);
  AuthCredentials copyWith(void Function(AuthCredentials) updates) => super.copyWith((message) => updates(message as AuthCredentials));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthCredentials create() => AuthCredentials._();
  AuthCredentials createEmptyInstance() => create();
  static $pb.PbList<AuthCredentials> createRepeated() => $pb.PbList<AuthCredentials>();
  @$core.pragma('dart2js:noInline')
  static AuthCredentials getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AuthCredentials>(create);
  static AuthCredentials _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get username => $_getSZ(0);
  @$pb.TagNumber(1)
  set username($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUsername() => $_has(0);
  @$pb.TagNumber(1)
  void clearUsername() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get role => $_getSZ(2);
  @$pb.TagNumber(3)
  set role($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRole() => $_has(2);
  @$pb.TagNumber(3)
  void clearRole() => clearField(3);
}

class AuthResult extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('AuthResult', package: const $pb.PackageName('auth'), createEmptyInstance: create)
    ..aOS(1, 'userId')
    ..aOS(2, 'accessToken')
    ..aOS(3, 'refreshToken')
    ..hasRequiredFields = false
  ;

  AuthResult._() : super();
  factory AuthResult() => create();
  factory AuthResult.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AuthResult.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  AuthResult clone() => AuthResult()..mergeFromMessage(this);
  AuthResult copyWith(void Function(AuthResult) updates) => super.copyWith((message) => updates(message as AuthResult));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static AuthResult create() => AuthResult._();
  AuthResult createEmptyInstance() => create();
  static $pb.PbList<AuthResult> createRepeated() => $pb.PbList<AuthResult>();
  @$core.pragma('dart2js:noInline')
  static AuthResult getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AuthResult>(create);
  static AuthResult _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userId => $_getSZ(0);
  @$pb.TagNumber(1)
  set userId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get accessToken => $_getSZ(1);
  @$pb.TagNumber(2)
  set accessToken($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasAccessToken() => $_has(1);
  @$pb.TagNumber(2)
  void clearAccessToken() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get refreshToken => $_getSZ(2);
  @$pb.TagNumber(3)
  set refreshToken($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRefreshToken() => $_has(2);
  @$pb.TagNumber(3)
  void clearRefreshToken() => clearField(3);
}

class VerifiedUser extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('VerifiedUser', package: const $pb.PackageName('auth'), createEmptyInstance: create)
    ..aOS(1, 'UserId', protoName: 'UserId')
    ..aOS(2, 'username')
    ..a<$core.List<$core.int>>(3, 'password', $pb.PbFieldType.OY)
    ..aOS(4, 'role')
    ..hasRequiredFields = false
  ;

  VerifiedUser._() : super();
  factory VerifiedUser() => create();
  factory VerifiedUser.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory VerifiedUser.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  VerifiedUser clone() => VerifiedUser()..mergeFromMessage(this);
  VerifiedUser copyWith(void Function(VerifiedUser) updates) => super.copyWith((message) => updates(message as VerifiedUser));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static VerifiedUser create() => VerifiedUser._();
  VerifiedUser createEmptyInstance() => create();
  static $pb.PbList<VerifiedUser> createRepeated() => $pb.PbList<VerifiedUser>();
  @$core.pragma('dart2js:noInline')
  static VerifiedUser getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<VerifiedUser>(create);
  static VerifiedUser _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get userId => $_getSZ(0);
  @$pb.TagNumber(1)
  set userId($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasUserId() => $_has(0);
  @$pb.TagNumber(1)
  void clearUserId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get username => $_getSZ(1);
  @$pb.TagNumber(2)
  set username($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUsername() => $_has(1);
  @$pb.TagNumber(2)
  void clearUsername() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get password => $_getN(2);
  @$pb.TagNumber(3)
  set password($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get role => $_getSZ(3);
  @$pb.TagNumber(4)
  set role($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasRole() => $_has(3);
  @$pb.TagNumber(4)
  void clearRole() => clearField(4);
}

