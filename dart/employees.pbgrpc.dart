///
//  Generated code. Do not modify.
//  source: employees.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'employees.pb.dart' as $0;
export 'employees.pb.dart';

class EmployeesClient extends $grpc.Client {
  static final _$add = $grpc.ClientMethod<$0.Item, $0.Item>(
      '/employees.Employees/Add',
      ($0.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Item.fromBuffer(value));
  static final _$update = $grpc.ClientMethod<$0.Item, $0.Item>(
      '/employees.Employees/Update',
      ($0.Item value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Item.fromBuffer(value));
  static final _$list = $grpc.ClientMethod<$0.ListReq, $0.Items>(
      '/employees.Employees/List',
      ($0.ListReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Items.fromBuffer(value));
  static final _$search = $grpc.ClientMethod<$0.SearchReq, $0.Items>(
      '/employees.Employees/Search',
      ($0.SearchReq value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Items.fromBuffer(value));
  static final _$restore = $grpc.ClientMethod<$0.RestoreRequest, $0.Response>(
      '/employees.Employees/Restore',
      ($0.RestoreRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Response.fromBuffer(value));
  static final _$delete = $grpc.ClientMethod<$0.DeleteRequest, $0.Response>(
      '/employees.Employees/Delete',
      ($0.DeleteRequest value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Response.fromBuffer(value));

  EmployeesClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.Item> add($0.Item request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$add, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Item> update($0.Item request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$update, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Items> list($0.ListReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$list, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Items> search($0.SearchReq request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$search, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Response> restore($0.RestoreRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$restore, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Response> delete($0.DeleteRequest request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$delete, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class EmployeesServiceBase extends $grpc.Service {
  $core.String get $name => 'employees.Employees';

  EmployeesServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Item, $0.Item>(
        'Add',
        add_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Item.fromBuffer(value),
        ($0.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Item, $0.Item>(
        'Update',
        update_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Item.fromBuffer(value),
        ($0.Item value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.ListReq, $0.Items>(
        'List',
        list_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.ListReq.fromBuffer(value),
        ($0.Items value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.SearchReq, $0.Items>(
        'Search',
        search_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.SearchReq.fromBuffer(value),
        ($0.Items value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.RestoreRequest, $0.Response>(
        'Restore',
        restore_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.RestoreRequest.fromBuffer(value),
        ($0.Response value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.DeleteRequest, $0.Response>(
        'Delete',
        delete_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.DeleteRequest.fromBuffer(value),
        ($0.Response value) => value.writeToBuffer()));
  }

  $async.Future<$0.Item> add_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Item> request) async {
    return add(call, await request);
  }

  $async.Future<$0.Item> update_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Item> request) async {
    return update(call, await request);
  }

  $async.Future<$0.Items> list_Pre(
      $grpc.ServiceCall call, $async.Future<$0.ListReq> request) async {
    return list(call, await request);
  }

  $async.Future<$0.Items> search_Pre(
      $grpc.ServiceCall call, $async.Future<$0.SearchReq> request) async {
    return search(call, await request);
  }

  $async.Future<$0.Response> restore_Pre(
      $grpc.ServiceCall call, $async.Future<$0.RestoreRequest> request) async {
    return restore(call, await request);
  }

  $async.Future<$0.Response> delete_Pre(
      $grpc.ServiceCall call, $async.Future<$0.DeleteRequest> request) async {
    return delete(call, await request);
  }

  $async.Future<$0.Item> add($grpc.ServiceCall call, $0.Item request);
  $async.Future<$0.Item> update($grpc.ServiceCall call, $0.Item request);
  $async.Future<$0.Items> list($grpc.ServiceCall call, $0.ListReq request);
  $async.Future<$0.Items> search($grpc.ServiceCall call, $0.SearchReq request);
  $async.Future<$0.Response> restore(
      $grpc.ServiceCall call, $0.RestoreRequest request);
  $async.Future<$0.Response> delete(
      $grpc.ServiceCall call, $0.DeleteRequest request);
}
