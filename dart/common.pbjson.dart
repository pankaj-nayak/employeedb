///
//  Generated code. Do not modify.
//  source: common.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

const EmptyMessage$json = const {
  '1': 'EmptyMessage',
};

const UUID$json = const {
  '1': 'UUID',
  '2': const [
    const {'1': 'val', '3': 1, '4': 1, '5': 9, '10': 'val'},
  ],
};

const UUIDs$json = const {
  '1': 'UUIDs',
  '2': const [
    const {'1': 'vals', '3': 1, '4': 3, '5': 9, '10': 'vals'},
  ],
};

