///
//  Generated code. Do not modify.
//  source: auth.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'auth.pb.dart' as $0;
export 'auth.pb.dart';

class AuthClient extends $grpc.Client {
  static final _$login = $grpc.ClientMethod<$0.AuthCredentials, $0.AuthResult>(
      '/auth.Auth/Login',
      ($0.AuthCredentials value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.AuthResult.fromBuffer(value));
  static final _$refreshToken =
      $grpc.ClientMethod<$0.AuthResult, $0.AuthResult>(
          '/auth.Auth/RefreshToken',
          ($0.AuthResult value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.AuthResult.fromBuffer(value));

  AuthClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.AuthResult> login($0.AuthCredentials request,
      {$grpc.CallOptions options}) {
    final call = $createCall(_$login, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.AuthResult> refreshToken($0.AuthResult request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$refreshToken, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class AuthServiceBase extends $grpc.Service {
  $core.String get $name => 'auth.Auth';

  AuthServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.AuthCredentials, $0.AuthResult>(
        'Login',
        login_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AuthCredentials.fromBuffer(value),
        ($0.AuthResult value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.AuthResult, $0.AuthResult>(
        'RefreshToken',
        refreshToken_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.AuthResult.fromBuffer(value),
        ($0.AuthResult value) => value.writeToBuffer()));
  }

  $async.Future<$0.AuthResult> login_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AuthCredentials> request) async {
    return login(call, await request);
  }

  $async.Future<$0.AuthResult> refreshToken_Pre(
      $grpc.ServiceCall call, $async.Future<$0.AuthResult> request) async {
    return refreshToken(call, await request);
  }

  $async.Future<$0.AuthResult> login(
      $grpc.ServiceCall call, $0.AuthCredentials request);
  $async.Future<$0.AuthResult> refreshToken(
      $grpc.ServiceCall call, $0.AuthResult request);
}
