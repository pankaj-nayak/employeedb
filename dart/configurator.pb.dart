///
//  Generated code. Do not modify.
//  source: configurator.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'configurator.pbenum.dart';

export 'configurator.pbenum.dart';

class ConfigReq extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ConfigReq', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..e<ConfigType>(1, 'confType', $pb.PbFieldType.OE, defaultOrMaker: ConfigType.RESERVED, valueOf: ConfigType.valueOf, enumValues: ConfigType.values)
    ..aOS(2, 'handler')
    ..pc<ConfigReq>(3, 'subConfs', $pb.PbFieldType.PM, subBuilder: ConfigReq.create)
    ..hasRequiredFields = false
  ;

  ConfigReq._() : super();
  factory ConfigReq() => create();
  factory ConfigReq.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConfigReq.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ConfigReq clone() => ConfigReq()..mergeFromMessage(this);
  ConfigReq copyWith(void Function(ConfigReq) updates) => super.copyWith((message) => updates(message as ConfigReq));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConfigReq create() => ConfigReq._();
  ConfigReq createEmptyInstance() => create();
  static $pb.PbList<ConfigReq> createRepeated() => $pb.PbList<ConfigReq>();
  @$core.pragma('dart2js:noInline')
  static ConfigReq getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConfigReq>(create);
  static ConfigReq _defaultInstance;

  @$pb.TagNumber(1)
  ConfigType get confType => $_getN(0);
  @$pb.TagNumber(1)
  set confType(ConfigType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasConfType() => $_has(0);
  @$pb.TagNumber(1)
  void clearConfType() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get handler => $_getSZ(1);
  @$pb.TagNumber(2)
  set handler($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasHandler() => $_has(1);
  @$pb.TagNumber(2)
  void clearHandler() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<ConfigReq> get subConfs => $_getList(2);
}

enum Config_Type {
  server, 
  ep, 
  db, 
  notSet
}

class Config extends $pb.GeneratedMessage {
  static const $core.Map<$core.int, Config_Type> _Config_TypeByTag = {
    2 : Config_Type.server,
    3 : Config_Type.ep,
    4 : Config_Type.db,
    0 : Config_Type.notSet
  };
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Config', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..oo(0, [2, 3, 4])
    ..e<ConfigType>(1, 'confType', $pb.PbFieldType.OE, defaultOrMaker: ConfigType.RESERVED, valueOf: ConfigType.valueOf, enumValues: ConfigType.values)
    ..aOM<GrpcServerConfig>(2, 'server', subBuilder: GrpcServerConfig.create)
    ..aOM<ProxyEndpointConfig>(3, 'ep', subBuilder: ProxyEndpointConfig.create)
    ..aOM<DBConfig>(4, 'db', subBuilder: DBConfig.create)
    ..hasRequiredFields = false
  ;

  Config._() : super();
  factory Config() => create();
  factory Config.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Config.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Config clone() => Config()..mergeFromMessage(this);
  Config copyWith(void Function(Config) updates) => super.copyWith((message) => updates(message as Config));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Config create() => Config._();
  Config createEmptyInstance() => create();
  static $pb.PbList<Config> createRepeated() => $pb.PbList<Config>();
  @$core.pragma('dart2js:noInline')
  static Config getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Config>(create);
  static Config _defaultInstance;

  Config_Type whichType() => _Config_TypeByTag[$_whichOneof(0)];
  void clearType() => clearField($_whichOneof(0));

  @$pb.TagNumber(1)
  ConfigType get confType => $_getN(0);
  @$pb.TagNumber(1)
  set confType(ConfigType v) { setField(1, v); }
  @$pb.TagNumber(1)
  $core.bool hasConfType() => $_has(0);
  @$pb.TagNumber(1)
  void clearConfType() => clearField(1);

  @$pb.TagNumber(2)
  GrpcServerConfig get server => $_getN(1);
  @$pb.TagNumber(2)
  set server(GrpcServerConfig v) { setField(2, v); }
  @$pb.TagNumber(2)
  $core.bool hasServer() => $_has(1);
  @$pb.TagNumber(2)
  void clearServer() => clearField(2);
  @$pb.TagNumber(2)
  GrpcServerConfig ensureServer() => $_ensure(1);

  @$pb.TagNumber(3)
  ProxyEndpointConfig get ep => $_getN(2);
  @$pb.TagNumber(3)
  set ep(ProxyEndpointConfig v) { setField(3, v); }
  @$pb.TagNumber(3)
  $core.bool hasEp() => $_has(2);
  @$pb.TagNumber(3)
  void clearEp() => clearField(3);
  @$pb.TagNumber(3)
  ProxyEndpointConfig ensureEp() => $_ensure(2);

  @$pb.TagNumber(4)
  DBConfig get db => $_getN(3);
  @$pb.TagNumber(4)
  set db(DBConfig v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasDb() => $_has(3);
  @$pb.TagNumber(4)
  void clearDb() => clearField(4);
  @$pb.TagNumber(4)
  DBConfig ensureDb() => $_ensure(3);
}

class ConfigurationsList extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ConfigurationsList', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..pc<Configurations>(1, 'conf', $pb.PbFieldType.PM, subBuilder: Configurations.create)
    ..hasRequiredFields = false
  ;

  ConfigurationsList._() : super();
  factory ConfigurationsList() => create();
  factory ConfigurationsList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ConfigurationsList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ConfigurationsList clone() => ConfigurationsList()..mergeFromMessage(this);
  ConfigurationsList copyWith(void Function(ConfigurationsList) updates) => super.copyWith((message) => updates(message as ConfigurationsList));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ConfigurationsList create() => ConfigurationsList._();
  ConfigurationsList createEmptyInstance() => create();
  static $pb.PbList<ConfigurationsList> createRepeated() => $pb.PbList<ConfigurationsList>();
  @$core.pragma('dart2js:noInline')
  static ConfigurationsList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ConfigurationsList>(create);
  static ConfigurationsList _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<Configurations> get conf => $_getList(0);
}

class Configurations extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Configurations', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..aOS(1, 'svcName')
    ..aOS(2, 'version')
    ..pc<Config>(3, 'configValues', $pb.PbFieldType.PM, subBuilder: Config.create)
    ..aInt64(98, 'created')
    ..aInt64(99, 'updated')
    ..hasRequiredFields = false
  ;

  Configurations._() : super();
  factory Configurations() => create();
  factory Configurations.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Configurations.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Configurations clone() => Configurations()..mergeFromMessage(this);
  Configurations copyWith(void Function(Configurations) updates) => super.copyWith((message) => updates(message as Configurations));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Configurations create() => Configurations._();
  Configurations createEmptyInstance() => create();
  static $pb.PbList<Configurations> createRepeated() => $pb.PbList<Configurations>();
  @$core.pragma('dart2js:noInline')
  static Configurations getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Configurations>(create);
  static Configurations _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get svcName => $_getSZ(0);
  @$pb.TagNumber(1)
  set svcName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSvcName() => $_has(0);
  @$pb.TagNumber(1)
  void clearSvcName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get version => $_getSZ(1);
  @$pb.TagNumber(2)
  set version($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVersion() => $_has(1);
  @$pb.TagNumber(2)
  void clearVersion() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<Config> get configValues => $_getList(2);

  @$pb.TagNumber(98)
  $fixnum.Int64 get created => $_getI64(3);
  @$pb.TagNumber(98)
  set created($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(98)
  $core.bool hasCreated() => $_has(3);
  @$pb.TagNumber(98)
  void clearCreated() => clearField(98);

  @$pb.TagNumber(99)
  $fixnum.Int64 get updated => $_getI64(4);
  @$pb.TagNumber(99)
  set updated($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(99)
  $core.bool hasUpdated() => $_has(4);
  @$pb.TagNumber(99)
  void clearUpdated() => clearField(99);
}

class Request extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('Request', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..aOS(1, 'svcName')
    ..aOS(2, 'version')
    ..pc<ConfigReq>(3, 'configs', $pb.PbFieldType.PM, subBuilder: ConfigReq.create)
    ..hasRequiredFields = false
  ;

  Request._() : super();
  factory Request() => create();
  factory Request.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Request.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  Request clone() => Request()..mergeFromMessage(this);
  Request copyWith(void Function(Request) updates) => super.copyWith((message) => updates(message as Request));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static Request create() => Request._();
  Request createEmptyInstance() => create();
  static $pb.PbList<Request> createRepeated() => $pb.PbList<Request>();
  @$core.pragma('dart2js:noInline')
  static Request getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Request>(create);
  static Request _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get svcName => $_getSZ(0);
  @$pb.TagNumber(1)
  set svcName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSvcName() => $_has(0);
  @$pb.TagNumber(1)
  void clearSvcName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get version => $_getSZ(1);
  @$pb.TagNumber(2)
  set version($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasVersion() => $_has(1);
  @$pb.TagNumber(2)
  void clearVersion() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<ConfigReq> get configs => $_getList(2);
}

class GrpcServerConfig extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('GrpcServerConfig', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..aOS(1, 'svcName')
    ..aOB(2, 'useTls')
    ..aOB(3, 'useJwt')
    ..aOB(4, 'useValidator')
    ..aOB(5, 'useRecovery')
    ..a<$core.int>(6, 'port', $pb.PbFieldType.O3)
    ..a<$core.int>(7, 'logLevel', $pb.PbFieldType.O3)
    ..aOS(8, 'certFile')
    ..aOS(9, 'keyFile')
    ..aOS(10, 'pubKeyFile')
    ..aOS(11, 'privKeyFile')
    ..pc<Config>(12, 'subConfs', $pb.PbFieldType.PM, subBuilder: Config.create)
    ..aOB(13, 'useTracing')
    ..aOS(14, 'tracingHost')
    ..aInt64(98, 'created')
    ..aInt64(99, 'updated')
    ..hasRequiredFields = false
  ;

  GrpcServerConfig._() : super();
  factory GrpcServerConfig() => create();
  factory GrpcServerConfig.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory GrpcServerConfig.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  GrpcServerConfig clone() => GrpcServerConfig()..mergeFromMessage(this);
  GrpcServerConfig copyWith(void Function(GrpcServerConfig) updates) => super.copyWith((message) => updates(message as GrpcServerConfig));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static GrpcServerConfig create() => GrpcServerConfig._();
  GrpcServerConfig createEmptyInstance() => create();
  static $pb.PbList<GrpcServerConfig> createRepeated() => $pb.PbList<GrpcServerConfig>();
  @$core.pragma('dart2js:noInline')
  static GrpcServerConfig getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<GrpcServerConfig>(create);
  static GrpcServerConfig _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get svcName => $_getSZ(0);
  @$pb.TagNumber(1)
  set svcName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSvcName() => $_has(0);
  @$pb.TagNumber(1)
  void clearSvcName() => clearField(1);

  @$pb.TagNumber(2)
  $core.bool get useTls => $_getBF(1);
  @$pb.TagNumber(2)
  set useTls($core.bool v) { $_setBool(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasUseTls() => $_has(1);
  @$pb.TagNumber(2)
  void clearUseTls() => clearField(2);

  @$pb.TagNumber(3)
  $core.bool get useJwt => $_getBF(2);
  @$pb.TagNumber(3)
  set useJwt($core.bool v) { $_setBool(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasUseJwt() => $_has(2);
  @$pb.TagNumber(3)
  void clearUseJwt() => clearField(3);

  @$pb.TagNumber(4)
  $core.bool get useValidator => $_getBF(3);
  @$pb.TagNumber(4)
  set useValidator($core.bool v) { $_setBool(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUseValidator() => $_has(3);
  @$pb.TagNumber(4)
  void clearUseValidator() => clearField(4);

  @$pb.TagNumber(5)
  $core.bool get useRecovery => $_getBF(4);
  @$pb.TagNumber(5)
  set useRecovery($core.bool v) { $_setBool(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasUseRecovery() => $_has(4);
  @$pb.TagNumber(5)
  void clearUseRecovery() => clearField(5);

  @$pb.TagNumber(6)
  $core.int get port => $_getIZ(5);
  @$pb.TagNumber(6)
  set port($core.int v) { $_setSignedInt32(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPort() => $_has(5);
  @$pb.TagNumber(6)
  void clearPort() => clearField(6);

  @$pb.TagNumber(7)
  $core.int get logLevel => $_getIZ(6);
  @$pb.TagNumber(7)
  set logLevel($core.int v) { $_setSignedInt32(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasLogLevel() => $_has(6);
  @$pb.TagNumber(7)
  void clearLogLevel() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get certFile => $_getSZ(7);
  @$pb.TagNumber(8)
  set certFile($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasCertFile() => $_has(7);
  @$pb.TagNumber(8)
  void clearCertFile() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get keyFile => $_getSZ(8);
  @$pb.TagNumber(9)
  set keyFile($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasKeyFile() => $_has(8);
  @$pb.TagNumber(9)
  void clearKeyFile() => clearField(9);

  @$pb.TagNumber(10)
  $core.String get pubKeyFile => $_getSZ(9);
  @$pb.TagNumber(10)
  set pubKeyFile($core.String v) { $_setString(9, v); }
  @$pb.TagNumber(10)
  $core.bool hasPubKeyFile() => $_has(9);
  @$pb.TagNumber(10)
  void clearPubKeyFile() => clearField(10);

  @$pb.TagNumber(11)
  $core.String get privKeyFile => $_getSZ(10);
  @$pb.TagNumber(11)
  set privKeyFile($core.String v) { $_setString(10, v); }
  @$pb.TagNumber(11)
  $core.bool hasPrivKeyFile() => $_has(10);
  @$pb.TagNumber(11)
  void clearPrivKeyFile() => clearField(11);

  @$pb.TagNumber(12)
  $core.List<Config> get subConfs => $_getList(11);

  @$pb.TagNumber(13)
  $core.bool get useTracing => $_getBF(12);
  @$pb.TagNumber(13)
  set useTracing($core.bool v) { $_setBool(12, v); }
  @$pb.TagNumber(13)
  $core.bool hasUseTracing() => $_has(12);
  @$pb.TagNumber(13)
  void clearUseTracing() => clearField(13);

  @$pb.TagNumber(14)
  $core.String get tracingHost => $_getSZ(13);
  @$pb.TagNumber(14)
  set tracingHost($core.String v) { $_setString(13, v); }
  @$pb.TagNumber(14)
  $core.bool hasTracingHost() => $_has(13);
  @$pb.TagNumber(14)
  void clearTracingHost() => clearField(14);

  @$pb.TagNumber(98)
  $fixnum.Int64 get created => $_getI64(14);
  @$pb.TagNumber(98)
  set created($fixnum.Int64 v) { $_setInt64(14, v); }
  @$pb.TagNumber(98)
  $core.bool hasCreated() => $_has(14);
  @$pb.TagNumber(98)
  void clearCreated() => clearField(98);

  @$pb.TagNumber(99)
  $fixnum.Int64 get updated => $_getI64(15);
  @$pb.TagNumber(99)
  set updated($fixnum.Int64 v) { $_setInt64(15, v); }
  @$pb.TagNumber(99)
  $core.bool hasUpdated() => $_has(15);
  @$pb.TagNumber(99)
  void clearUpdated() => clearField(99);
}

class ProxyEndpointConfig_Endpoint extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ProxyEndpointConfig.Endpoint', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..aOS(1, 'svcName')
    ..aOS(2, 'hostname')
    ..hasRequiredFields = false
  ;

  ProxyEndpointConfig_Endpoint._() : super();
  factory ProxyEndpointConfig_Endpoint() => create();
  factory ProxyEndpointConfig_Endpoint.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ProxyEndpointConfig_Endpoint.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ProxyEndpointConfig_Endpoint clone() => ProxyEndpointConfig_Endpoint()..mergeFromMessage(this);
  ProxyEndpointConfig_Endpoint copyWith(void Function(ProxyEndpointConfig_Endpoint) updates) => super.copyWith((message) => updates(message as ProxyEndpointConfig_Endpoint));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ProxyEndpointConfig_Endpoint create() => ProxyEndpointConfig_Endpoint._();
  ProxyEndpointConfig_Endpoint createEmptyInstance() => create();
  static $pb.PbList<ProxyEndpointConfig_Endpoint> createRepeated() => $pb.PbList<ProxyEndpointConfig_Endpoint>();
  @$core.pragma('dart2js:noInline')
  static ProxyEndpointConfig_Endpoint getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ProxyEndpointConfig_Endpoint>(create);
  static ProxyEndpointConfig_Endpoint _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get svcName => $_getSZ(0);
  @$pb.TagNumber(1)
  set svcName($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSvcName() => $_has(0);
  @$pb.TagNumber(1)
  void clearSvcName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get hostname => $_getSZ(1);
  @$pb.TagNumber(2)
  set hostname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasHostname() => $_has(1);
  @$pb.TagNumber(2)
  void clearHostname() => clearField(2);
}

class ProxyEndpointConfig extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('ProxyEndpointConfig', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..pc<ProxyEndpointConfig_Endpoint>(1, 'endpoints', $pb.PbFieldType.PM, subBuilder: ProxyEndpointConfig_Endpoint.create)
    ..aOS(2, 'svcName')
    ..a<$core.int>(3, 'port', $pb.PbFieldType.O3)
    ..aInt64(98, 'created')
    ..aInt64(99, 'updated')
    ..hasRequiredFields = false
  ;

  ProxyEndpointConfig._() : super();
  factory ProxyEndpointConfig() => create();
  factory ProxyEndpointConfig.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory ProxyEndpointConfig.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  ProxyEndpointConfig clone() => ProxyEndpointConfig()..mergeFromMessage(this);
  ProxyEndpointConfig copyWith(void Function(ProxyEndpointConfig) updates) => super.copyWith((message) => updates(message as ProxyEndpointConfig));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static ProxyEndpointConfig create() => ProxyEndpointConfig._();
  ProxyEndpointConfig createEmptyInstance() => create();
  static $pb.PbList<ProxyEndpointConfig> createRepeated() => $pb.PbList<ProxyEndpointConfig>();
  @$core.pragma('dart2js:noInline')
  static ProxyEndpointConfig getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<ProxyEndpointConfig>(create);
  static ProxyEndpointConfig _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<ProxyEndpointConfig_Endpoint> get endpoints => $_getList(0);

  @$pb.TagNumber(2)
  $core.String get svcName => $_getSZ(1);
  @$pb.TagNumber(2)
  set svcName($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasSvcName() => $_has(1);
  @$pb.TagNumber(2)
  void clearSvcName() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get port => $_getIZ(2);
  @$pb.TagNumber(3)
  set port($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPort() => $_has(2);
  @$pb.TagNumber(3)
  void clearPort() => clearField(3);

  @$pb.TagNumber(98)
  $fixnum.Int64 get created => $_getI64(3);
  @$pb.TagNumber(98)
  set created($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(98)
  $core.bool hasCreated() => $_has(3);
  @$pb.TagNumber(98)
  void clearCreated() => clearField(98);

  @$pb.TagNumber(99)
  $fixnum.Int64 get updated => $_getI64(4);
  @$pb.TagNumber(99)
  set updated($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(99)
  $core.bool hasUpdated() => $_has(4);
  @$pb.TagNumber(99)
  void clearUpdated() => clearField(99);
}

class DBConfig extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo('DBConfig', package: const $pb.PackageName('configurator'), createEmptyInstance: create)
    ..aOS(1, 'handler')
    ..aOS(2, 'hostname')
    ..a<$core.int>(3, 'port', $pb.PbFieldType.O3)
    ..aOS(4, 'username')
    ..aOS(5, 'password')
    ..aOS(6, 'dbName')
    ..aOS(7, 'dbPath')
    ..aInt64(98, 'created')
    ..aInt64(99, 'updated')
    ..hasRequiredFields = false
  ;

  DBConfig._() : super();
  factory DBConfig() => create();
  factory DBConfig.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DBConfig.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  DBConfig clone() => DBConfig()..mergeFromMessage(this);
  DBConfig copyWith(void Function(DBConfig) updates) => super.copyWith((message) => updates(message as DBConfig));
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static DBConfig create() => DBConfig._();
  DBConfig createEmptyInstance() => create();
  static $pb.PbList<DBConfig> createRepeated() => $pb.PbList<DBConfig>();
  @$core.pragma('dart2js:noInline')
  static DBConfig getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DBConfig>(create);
  static DBConfig _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get handler => $_getSZ(0);
  @$pb.TagNumber(1)
  set handler($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasHandler() => $_has(0);
  @$pb.TagNumber(1)
  void clearHandler() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get hostname => $_getSZ(1);
  @$pb.TagNumber(2)
  set hostname($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasHostname() => $_has(1);
  @$pb.TagNumber(2)
  void clearHostname() => clearField(2);

  @$pb.TagNumber(3)
  $core.int get port => $_getIZ(2);
  @$pb.TagNumber(3)
  set port($core.int v) { $_setSignedInt32(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPort() => $_has(2);
  @$pb.TagNumber(3)
  void clearPort() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get username => $_getSZ(3);
  @$pb.TagNumber(4)
  set username($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasUsername() => $_has(3);
  @$pb.TagNumber(4)
  void clearUsername() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get password => $_getSZ(4);
  @$pb.TagNumber(5)
  set password($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPassword() => $_has(4);
  @$pb.TagNumber(5)
  void clearPassword() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get dbName => $_getSZ(5);
  @$pb.TagNumber(6)
  set dbName($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDbName() => $_has(5);
  @$pb.TagNumber(6)
  void clearDbName() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get dbPath => $_getSZ(6);
  @$pb.TagNumber(7)
  set dbPath($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasDbPath() => $_has(6);
  @$pb.TagNumber(7)
  void clearDbPath() => clearField(7);

  @$pb.TagNumber(98)
  $fixnum.Int64 get created => $_getI64(7);
  @$pb.TagNumber(98)
  set created($fixnum.Int64 v) { $_setInt64(7, v); }
  @$pb.TagNumber(98)
  $core.bool hasCreated() => $_has(7);
  @$pb.TagNumber(98)
  void clearCreated() => clearField(98);

  @$pb.TagNumber(99)
  $fixnum.Int64 get updated => $_getI64(8);
  @$pb.TagNumber(99)
  set updated($fixnum.Int64 v) { $_setInt64(8, v); }
  @$pb.TagNumber(99)
  $core.bool hasUpdated() => $_has(8);
  @$pb.TagNumber(99)
  void clearUpdated() => clearField(99);
}

