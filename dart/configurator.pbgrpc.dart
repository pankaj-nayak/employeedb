///
//  Generated code. Do not modify.
//  source: configurator.proto
//
// @dart = 2.3
// ignore_for_file: camel_case_types,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type

import 'dart:async' as $async;

import 'dart:core' as $core;

import 'package:grpc/service_api.dart' as $grpc;
import 'configurator.pb.dart' as $0;
import 'common.pb.dart' as $1;
export 'configurator.pb.dart';

class ConfiguratorClient extends $grpc.Client {
  static final _$getConfigurations =
      $grpc.ClientMethod<$0.Request, $0.Configurations>(
          '/configurator.Configurator/GetConfigurations',
          ($0.Request value) => value.writeToBuffer(),
          ($core.List<$core.int> value) => $0.Configurations.fromBuffer(value));
  static final _$getServices =
      $grpc.ClientMethod<$1.EmptyMessage, $0.ConfigurationsList>(
          '/configurator.Configurator/GetServices',
          ($1.EmptyMessage value) => value.writeToBuffer(),
          ($core.List<$core.int> value) =>
              $0.ConfigurationsList.fromBuffer(value));
  static final _$updateConfig = $grpc.ClientMethod<$0.Config, $0.Config>(
      '/configurator.Configurator/UpdateConfig',
      ($0.Config value) => value.writeToBuffer(),
      ($core.List<$core.int> value) => $0.Config.fromBuffer(value));

  ConfiguratorClient($grpc.ClientChannel channel, {$grpc.CallOptions options})
      : super(channel, options: options);

  $grpc.ResponseFuture<$0.Configurations> getConfigurations($0.Request request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getConfigurations, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.ConfigurationsList> getServices(
      $1.EmptyMessage request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$getServices, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }

  $grpc.ResponseFuture<$0.Config> updateConfig($0.Config request,
      {$grpc.CallOptions options}) {
    final call = $createCall(
        _$updateConfig, $async.Stream.fromIterable([request]),
        options: options);
    return $grpc.ResponseFuture(call);
  }
}

abstract class ConfiguratorServiceBase extends $grpc.Service {
  $core.String get $name => 'configurator.Configurator';

  ConfiguratorServiceBase() {
    $addMethod($grpc.ServiceMethod<$0.Request, $0.Configurations>(
        'GetConfigurations',
        getConfigurations_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Request.fromBuffer(value),
        ($0.Configurations value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$1.EmptyMessage, $0.ConfigurationsList>(
        'GetServices',
        getServices_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $1.EmptyMessage.fromBuffer(value),
        ($0.ConfigurationsList value) => value.writeToBuffer()));
    $addMethod($grpc.ServiceMethod<$0.Config, $0.Config>(
        'UpdateConfig',
        updateConfig_Pre,
        false,
        false,
        ($core.List<$core.int> value) => $0.Config.fromBuffer(value),
        ($0.Config value) => value.writeToBuffer()));
  }

  $async.Future<$0.Configurations> getConfigurations_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Request> request) async {
    return getConfigurations(call, await request);
  }

  $async.Future<$0.ConfigurationsList> getServices_Pre(
      $grpc.ServiceCall call, $async.Future<$1.EmptyMessage> request) async {
    return getServices(call, await request);
  }

  $async.Future<$0.Config> updateConfig_Pre(
      $grpc.ServiceCall call, $async.Future<$0.Config> request) async {
    return updateConfig(call, await request);
  }

  $async.Future<$0.Configurations> getConfigurations(
      $grpc.ServiceCall call, $0.Request request);
  $async.Future<$0.ConfigurationsList> getServices(
      $grpc.ServiceCall call, $1.EmptyMessage request);
  $async.Future<$0.Config> updateConfig(
      $grpc.ServiceCall call, $0.Config request);
}
