module gitlab.com/employeedb

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/garyburd/redigo v1.6.2
	github.com/golang/protobuf v1.4.2
	github.com/google/martian v2.1.0+incompatible
	github.com/grpc-ecosystem/go-grpc-middleware v1.2.2
	github.com/grpc-ecosystem/grpc-gateway v1.15.2
	github.com/ipfs/go-log v1.0.4
	github.com/pkg/errors v0.8.1
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	golang.org/x/net v0.0.0-20200421231249-e086a090c8fd
	google.golang.org/genproto v0.0.0-20201013134114-7f9ee70cb474
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
)
