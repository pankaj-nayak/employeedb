package objects

import (
	"crypto/rsa"
	"log"

	grpc_middleware "github.com/grpc-ecosystem/go-grpc-middleware"
	grpc_auth "github.com/grpc-ecosystem/go-grpc-middleware/auth"
	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	app_errors "gitlab.com/employeedb/app-errors"
	"gitlab.com/employeedb/common/objects/utils"
	Configurator "gitlab.com/employeedb/configurator/pb"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"google.golang.org/grpc/metadata"
)

type GrpcServerConf struct {
	*Configurator.GrpcServerConfig

	PubKey        *rsa.PublicKey
	PrivKey       *rsa.PrivateKey
	auth_func_set bool
	auth_func     grpc_auth.AuthFunc
	recv_func_set bool
	recv_func     grpc_recovery.RecoveryHandlerFunc
	docker_mode   bool
}

func (c *GrpcServerConf) GetServerOpts() ([]grpc.ServerOption, error) {

	var opts []grpc.ServerOption

	if c.UseTls {
		creds, err := credentials.NewServerTLSFromFile(c.CertFile, c.KeyFile)
		if err != nil {
			log.Printf("Failed creating TLS credentials.ERR:%s\n", err)
			return opts, err
		}

		opts = append(opts, grpc.Creds(creds))
	}

	var u_interceptors []grpc.UnaryServerInterceptor
	var s_interceptors []grpc.StreamServerInterceptor

	if c.UseJwt {
		if !c.auth_func_set {
			err := c.withDefaultAuthFunc()
			if err != nil {
				log.Printf("Failed creating default JWT auth. ERR:%s\n", err.Error())
				return nil, err
			}
		}
		u_interceptors = append(u_interceptors, grpc_auth.UnaryServerInterceptor(c.auth_func))
		s_interceptors = append(s_interceptors, grpc_auth.StreamServerInterceptor(c.auth_func))

	}

	opts = append(opts, grpc_middleware.WithUnaryServerChain(u_interceptors...))
	opts = append(opts, grpc_middleware.WithStreamServerChain(s_interceptors...))

	return opts, nil
}

func (c *GrpcServerConf) DefaultAuthFunction(ctx context.Context) (context.Context, error) {

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return nil, app_errors.ErrUnauthenticated("Metadata corrupted")
	}

	jwtToken, ok := md["authorization"]
	if !ok {
		return nil, app_errors.ErrUnauthenticated("Authorization header not present")
	}

	token, err := utils.ValidateToken(jwtToken[0], c.PrivKey)
	if err != nil {
		return nil, app_errors.ErrUnauthenticated("Invalid token")
	}

	newCtx := context.WithValue(ctx, "jwt_token", token)
	claims, ok := token.Claims.(*utils.AuthClaims)
	if ok {
		newCtx = context.WithValue(newCtx, "user_id", claims.UserUuid)
	}
	return newCtx, nil
}

func (c *GrpcServerConf) MyAuthFunction(ctx context.Context, accessibleRoles []string) error {

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return app_errors.ErrUnauthenticated("Metadata corrupted")
	}

	jwtToken, ok := md["authorization"]
	if !ok {
		return app_errors.ErrUnauthenticated("Authorization header not present")
	}

	claims, err := utils.Verify(jwtToken[0])
	if err != nil {
		return app_errors.ErrUnauthenticated("Invalid token")
	}

	for _, role := range accessibleRoles {
		if role == claims.LoginType {
			return nil
		}
	}

	return app_errors.ErrPermissionDenied("no permission to access this RPC")
}

func (c *GrpcServerConf) withDefaultAuthFunc() error {

	c.auth_func = c.DefaultAuthFunction
	c.auth_func_set = true
	return nil
}

func matchDB(hdlr1, hdlr2 string) bool {
	switch hdlr1 {
	case "os_filestore_test":
		return hdlr2 == "os_filestore"
	}
	return hdlr1 == hdlr2
}

func (c *GrpcServerConf) GetDBConfig(handler string) *Configurator.DBConfig {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_DB_CONFIG {
			if matchDB(c.SubConfs[i].GetDb().Handler, handler) {
				return c.SubConfs[i].GetDb()
			}
		}
	}
	return nil
}

func (c *GrpcServerConf) GetSubServerConfig(svc string) *GrpcServerConf {
	for i := range c.SubConfs {
		if c.SubConfs[i].ConfType == Configurator.ConfigType_GRPC_SERVER_CONFIG {
			if c.SubConfs[i].GetServer().SvcName == svc {
				return &GrpcServerConf{GrpcServerConfig: c.SubConfs[i].GetServer()}
			}
		}
	}
	return nil
}
