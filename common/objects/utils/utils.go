package utils

import (
	"crypto/rsa"
	"fmt"

	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/google/martian/log"
)

type AuthClaims struct {
	*jwt.StandardClaims
	TokenType string
	UserUuid  string
	Username  string
	LoginType string
}

const (
	secretKey = "mysecretkey"
)

func Verify(accessToken string) (*AuthClaims, error) {
	token, err := jwt.ParseWithClaims(
		accessToken,
		&AuthClaims{},
		func(token *jwt.Token) (interface{}, error) {
			_, ok := token.Method.(*jwt.SigningMethodHMAC)
			if !ok {
				return nil, fmt.Errorf("unexpected token signing method")
			}

			return []byte(secretKey), nil
		},
	)

	if err != nil {
		return nil, fmt.Errorf("invalid token: %w", err)
	}

	claims, ok := token.Claims.(*AuthClaims)
	if !ok {
		return nil, fmt.Errorf("invalid token claims")
	}

	return claims, nil
}
func CreateTokens(user_id, username,
	loginType string) (accessToken string, refreshToken string, err error) {

	claim := AuthClaims{
		&jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Hour * 24).Unix()},
		"access_token", user_id, username, loginType,
	}

	newToken := jwt.NewWithClaims(jwt.SigningMethodHS256, claim)
	accessToken, err = newToken.SignedString([]byte(secretKey))
	if err != nil {
		return
	}

	claim2 := AuthClaims{
		&jwt.StandardClaims{ExpiresAt: time.Now().Add(time.Hour * 24 * 30).Unix()},
		"refresh_token", user_id, username, loginType,
	}
	newToken = jwt.NewWithClaims(jwt.SigningMethodHS256, claim2)
	refreshToken, err = newToken.SignedString([]byte(secretKey))
	if err != nil {
		accessToken = ""
		return
	}
	log.Infof("Generated New tokens: %s %s", accessToken, refreshToken)
	return
}

func ValidateToken(token string, publicKey *rsa.PrivateKey) (*jwt.Token, error) {
	jwtToken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		if _, ok := t.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Invalid token %s", token)
		}
		return []byte("secret"), nil
	})
	if err == nil && jwtToken.Valid {
		return jwtToken, nil
	}
	return nil, err
}
