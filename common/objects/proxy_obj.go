package objects

import Configurator "gitlab.com/employeedb/configurator/pb"

type ProxyConf struct {
	*Configurator.ProxyEndpointConfig
}

func (p *ProxyConf) GetProxyEp(svc string) *Configurator.ProxyEndpointConfig_Endpoint {
	for _, v := range p.Endpoints {
		if v.SvcName == svc {
			return v
		}
	}
	return nil
}
