GRPC and HTTP json based Employee database

STEPS to run : 

1. Using DOCKER-COMPOSE file. 
    1. Clone this project using -> git clone https://gitlab.com/pankaj-nayak/employeedb.git.
    2. cd employeedb
    3. docker-compose up --> This will start two servers : a) Http proxy server on port 8081 & b) Grpc Server on port 10000
    4. Call http://localhost:8081/login with POST method type with following paramters in body to get **accessToken & refreshToken**.
    {
    "username" :"admin",
    "password" : "secret",
    "role" : "admin"
    }
    or 
    curl --location --request POST 'http://localhost:8081/login' --header 'Content-Type: application/json' --data-raw ' {"username" :"admin","password" : "secret","role" : "admin"}' to get accessToken.
    5. Use this accessToken in authentication of headerrequest of POST type to accesss "add", "update","search", "list", "delete", & "restore" commands on employee service. 

    Please see example for each below : 

1. Add employee :
curl --location --request POST 'http://localhost:8081/add' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{
   "name" :"pankaj",
   "department": "DEVELOPMENT",
   "location": "koramangala",
   "skills" : ["golang","dart","c++"],
   "active" :"TRUE"
}'



2. Update : 
curl --location --request POST 'http://localhost:8081/update' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{  "id" : "adfdfdff-b79d-46f4-94a8-f54d30035642",
   "name" :"pankaj",
   "department": "DEVELOPMENT",
   "location": "koramangala",
   "skills" : ["golddang","dart","c++"],
   "active" :"TRUE"
}'

3. search : 
curl --location --request POST 'http://localhost:8081/search' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{
   
   "value" :""
}'

4. list : 
curl --location --request POST 'http://localhost:8081/list' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{
   "id": ["adfdfdff-b79d-46f4-94a8-f54d30035642"],
   "department": "DEVELOPMENT",
   "skills" : "dart"
   
}'


5. delete : 
 curl --location --request POST 'http://localhost:8081/delete' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{
   "id": "adfdfdff-b79d-46f4-94a8-f54d30035642"
}'

6. delete permanently : 
curl --location --request POST 'http://localhost:8081/delete' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{
   "id": "adfdfdff-b79d-46f4-94a8-f54d30035642",
   "deletepermanently" : "TRUE"
   
   
}'

7. Restore : 
curl --location --request POST 'http://localhost:8081/restore' \
--header 'Host: 162.210.101.156' \
--header 'Authorization: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2MDI2OTM1NDksIlRva2VuVHlwZSI6ImFjY2Vzc190b2tlbiIsIlVzZXJVdWlkIjoiMTM5OTM3OWItMjQ5Yy00ZmExLWFlMDMtZjIyZjA2MTg2N2I0IiwiVXNlcm5hbWUiOiJhZG1pbiIsIkxvZ2luVHlwZSI6ImFkbWluIn0.y3yaMCBFRP4kLHFar7cbNLPMkAfPYgUdFViebS-xmX4' \
--header 'Content-Type: application/json' \
--data-raw '{
    "id" :"6c345a1c-8578-4873-bed5-a7e692274bab"
}'



SCALABILITY TESTING : 
1. API is tested with 500 concurrent call with 500 employee records present in the dataabase. 
:~ jmeter -n -t Jmeter.jmx 
WARNING: package sun.awt.X11 not in java.desktop
Creating summariser <summary>
Created the tree successfully using Jmeter.jmx
Starting standalone test @ Tue Oct 13 23:51:21 IST 2020 (1602613281724)
Waiting for possible Shutdown/StopTestNow/HeapDump/ThreadDump message on port 4445
Warning: Nashorn engine is planned to be removed from a future JDK release

**summary +    235 in 00:00:08 =   30.2/s Avg:  4357 Min:  1416 Max:  7391 Err:     0 (0.00%) Active: 266 Started: 500 Finished: 234

summary +    265 in 00:00:05 =   56.3/s Avg:  9310 Min:  6492 Max: 11557 Err:     0 (0.00%) Active: 0 Started: 500 Finished: 500

summary =    500 in 00:00:12 =   40.1/s Avg:  6982 Min:  1416 Max: 11557 Err:     0 (0.00%)**


Tidying up ...    @ Tue Oct 13 23:51:34 IST 2020 (1602613294715)

... end of run


2. Using GO RUN command 
   1. redis-server needs to installed on the local machine.  
   2. run using "go run server.go" to start the server . 
   3. Follow above steps to execute "login" to get accessToken and then call employee services. 


